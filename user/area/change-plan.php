<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once "../stripe-init.php";
// if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != true) {
//     header('location: /index.php');
// }
include_once '../header.php';
?>

<?php
include_once "../../shared/topbar.php";
?>
<div class="stickyfooter ">
    <div class="breadcrumb block--medium">
        <a href="dashboard.php" class="breadcrumb__item">Dashboard</a>
        <a href="settings.php" class="breadcrumb__item">Settings</a>
        <span class="breadcrumb__item">Change Suscription</span>
    </div>
    <section class="block block--payment centered-body">
        <h2 class="block__title">Change suscription</h2>
        <form action="" method="post">
            <div class="block__body">
                <?php
$email = $_SESSION['email'];
$sql = "SELECT user_stripeId FROM users WHERE user_email='$email'";
$results = mysqli_query($conn, $sql);
if ($result = mysqli_query($conn, $sql)) {
    while ($row = mysqli_fetch_array($result)) {
        $stripe_id = $row['user_stripeId'];
    }

} else {
    echo "ERROR: a weird error happened. Try again please.";
}
$user = \Stripe\Customer::retrieve($stripe_id);

$user_current_period_end = $user->subscriptions->data[0]->current_period_end;
$user_subscription_id = $user->subscriptions->data[0]->id;
$user_plan_id = $user->subscriptions->data[0]->items->data[0]->plan->id;
$subscription = \Stripe\Subscription::retrieve($user_subscription_id);

if ($user_plan_id == $planannual) {
    $plan = "anual";
} else if ($user_plan_id == $plantrimestral) {
    $plan = "trimestral";
}

// Update plan
if (isset($_POST['suscription'])) {
    $error = false;
    try {
        if ($_POST['suscription'] == 1) {

            \Stripe\Subscription::update($user_subscription_id, [
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'plan' => $planannual,
                    ],
                ],
            ]);
        } else if ($_POST['suscription'] == 2) {
            \Stripe\Subscription::update($user_subscription_id, [
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'plan' => $plantrimestral,
                    ],
                ],
            ]);
        }
        $user = \Stripe\Customer::retrieve($stripe_id);

        $user_current_period_end = $user->subscriptions->data[0]->current_period_end;
        $user_subscription_id = $user->subscriptions->data[0]->id;
        $user_plan_id = $user->subscriptions->data[0]->items->data[0]->plan->id;
        $subscription = \Stripe\Subscription::retrieve($user_subscription_id);

        if ($user_plan_id == $planannual) {
            $plan = "anual";
        } else if ($user_plan_id == $plantrimestral) {
            $plan = "trimestral";
        }
        echo "<div class=\"alert--success\">Your plan has been Updated!</div>";
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
    echo $error;
}
?>
                <p>Changes will come effective after the current billing period ends. You will enjoy your current subscription until <b><?php echo date('Y-m-d', $user_current_period_end); ?></b></p>
                <div class="grid has-separator">
					<div class="checkbox grid--item-6">
						<input type="radio" id="anual" name="suscription" value="1" <?php if ($plan == "anual") {echo "checked=\"checked\"";}?> >
						<label class="checkbox__label" for="anual"><b>Annual suscription:</b> <small class="display-block">$20 billed every year </small></label>
					</div>
					<div class="checkbox grid--item-6">
						<input type="radio" id="trimester" name="suscription" value="2" <?php if ($plan == "trimestral") {echo "checked=\"checked\"";}?>>
						<label class="checkbox__label" for="trimester"><b>3 month suscription:</b><small class="display-block"> $10 billed every threee months </small></label>
					</div>
                </div>

            </div>
            <div class="block__footer">
                <a href="../settings.php" class="btn--secondary">Cancel</a> <input type="submit" value="Update Suscription" class="btn">
            </div>
        </form>
    </section>
</div>

<?php
include_once "../../shared/footer.php";
?>

</body>
</html>
