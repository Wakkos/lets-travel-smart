var stripe = Stripe('pk_test_I2S1nIeZ3srPaU0qC4m7PNvn');
var elements = stripe.elements();


// Create an instance of the card Element.
var card = elements.create('card', {
	iconStyle: 'solid',
    style: {
      base: {
        iconColor: 'black',
        color: '#586368',
        fontWeight: 500,
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',

        ':-webkit-autofill': {
          color: '#fce883',
        },
        '::placeholder': {
          color: '#bfbfbf',
        },
      },
      invalid: {
        iconColor: 'rgb(218,79,73)',
        color: 'rgb(218,79,73)',
      },
    },
});

// Add an instance of the card Element into the `card-element` <div>.
//card.mount('#card-element');


card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
    displayError.className += " alert--error";
  } else {
    displayError.textContent = '';
    displayError.classList.remove("alert--error");
  }
});


// Create a token or display an error when the form is submitted.
var form = document.getElementById('js-emailform');
form.addEventListener('submit', function(event) {
  event.preventDefault();
  let test = true;
if(!test){
  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
}else{
  stripeTokenHandler(makeid());
}
  
});

function makeid() {
  var text = "dummy_";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  var token = [];
  token.id = text;
  return token;
}

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('js-emailform');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

//Autocomplete function
var thief = document.getElementById('thief').innerHTML;
var embarrased = document.getElementById('embarrased').innerHTML;
var happiness = document.getElementById('happiness').innerHTML;
var indiferent = document.getElementById('indiferent').innerHTML;

// Pass Strength
var strength = {
  0: thief,
  1: thief,
  2: embarrased,
  3: indiferent,
  4: happiness
};
var password = document.getElementById('password');
var meter = document.getElementById('password-strength-meter');
var text = document.getElementById('password-strength-text');

password.addEventListener('input', function() {
  var val = password.value;
  var result = zxcvbn(val);

  // Update the password strength meter
  meter.value = result.score;

  // Update the text indicator
  if (val !== "") {
    text.innerHTML = strength[result.score];
  } else {
    text.innerHTML = "";
  }
});
