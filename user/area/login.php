<?php
include_once "../../helper/session.php";
include_once "../../phpscript/conn.php";
include_once "../stripe-init.php";
include_once "../header.php";
?>
<?php
$loginResult = "";
if (isset($_POST["loginSubmit"])) {
    $email = $_POST["loginemail"];
    $password = $_POST["loginpass"];

    if ("" == trim($email) || "" == trim($password)) {
        $loginResult .= "<div class='alert--error'>Email or password can't be blank.</div>";
    } else {
        $sql = "SELECT user_id, user_password FROM users WHERE user_email='$email'";
        if ($result = mysqli_query($conn, $sql)) {
            while ($row = mysqli_fetch_array($result)) {

                $id = $row['user_id'];
                $hased_pass = $row['user_password'];
                if (password_verify($password, $hased_pass)) {
                    $_SESSION['id'] = $id;
                    $_SESSION['loggedin'] = true;
                    $_SESSION['email'] = $email;
                    $email = "";
                    mysqli_free_result($result);
                    header('location: /user/dashboard.php');
                }
            }

            $loginResult .= '<div class="alert--error">No combination of user and password were found .</div>';

            // Free result set
            mysqli_free_result($result);
        } else {
            $loginResult .= "ERROR: Could not able to execute" . $sql . " " . mysqli_error($conn);
        }
    }

}
?>

<?php
include_once "../../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
        <h1 class="block__title align-center">Log in to your account.</h1>
        <div class="block__body">
            <?php
             echo $loginResult;
?>
            <form method="post">
                    <label for="loginemail">Email</label>
                    <input type="email" class="margin-bottom" placeholder="janedoe@gmail.com" name="loginemail" id="loginemail">
                    <label for="loginemail">Password</label>
                    <input type="password" placeholder="Must be at least 6 characters" name="loginpass">
        </div>
        <div class="block__footer">
            <a href="/" class="btn--secondary margin-bottom">Cancel</a>
            <input type="submit" value="Log In" class="btn margin-bottom" name="loginSubmit">
            <p class="font-small nomargin-bottom"><a href="/user/area/resetpass.php">Forgot</a> your password? | Do you want to <a href="/home/signup.php">Sign Up?</a></p>
        </div>
        </form>


	</section>
</div>
<?php
include_once "../../shared/footer.php";
?>

</body>
</html>