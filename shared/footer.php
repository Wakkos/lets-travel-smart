<footer class="footer">
<p>&copy; FlightMondo 2019</p>
<ul class="hor-list">
    <li class="hor-list__item"><a href="/home/faq.php">FAQ</a></li>
    <li class="hor-list__item"><a href="/home/legal/flight-deals-aboutus.php">About Us</a></li>
    <li class="hor-list__item"><a href="/home/legal/flight-deals-contact.php">Contact</a></li>
    <li class="hor-list__item"><a href="/home/prices.php">Prices</a></li>
    <li class="hor-list__item"><a href="https://privacypolicies.com/privacy/view/0197e64b179431ac0ff921a99bed60bc" target="_blank">Privacy Policy</a></li>
    <li class="hor-list__item"><a href="/home/legal/flight-deals-refund.php">Refund policy</a></li>
   <?php if (isset($_SESSION['email'])):?>

<?php else: ?>
    <li class="hor-list__item"><a href="/home/signup.php">Sign Up</a></li>
    <?php
endif;
?>
</ul>
</footer>
