<?php
include_once "../../helper/session.php";
include_once "../../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey">
<?php
include_once "../../shared/topbar.php";
?>

<section class="max-width padding-section stickyfooter">
    <h2 class="align-center margin-bottom-xl grid--item-12 ">Refund Policy</h2>
    <p>We stand by our product and offer a 30-day money-back guarantee, no questions asked. You can cancel your subscription any time. No hard feelings :).</p>
    <p><b>FlightMondo</b> is committed to customer satisfaction. We fundamentally believe you will be thrilled with the service you acquire from us. We understand, however, that sometimes a service may not be what you expected it to be. In that unlikely event, you can come back to us anytime within 30 days after becoming a Premium member. An easy refund is guaranteed.</p>
    <p>Please email us at refunds@flightmondo.com to request your money back, and your subscription will be immediately canceled.
Refunds can only be requested within 30 days of upgrading to a premium subscriber, thus automatic renewals are non-refundable, because they all occur after the initial 30-day refund period, and prorated returns will not be considered.</p>




</section>

<?php
include_once "../../shared/footer.php";
?>

</body>
</html>
