

<div class="topbar">
    <div class="topbar__logo">
        <a href="/">
        <img src="/img/logo.png" alt="Flightmondo Logo">

        </a>
        <link itemprop="logo" content="img/logo.png" />
    </div>
    <div class="flex-right grid align-center">
    <?php
if (isset($_SESSION['email'])):
?>
        <a href="/user/area/logout.php" class="btn margin-sides">Log Out</a>
        <a class="topbar__icon" href="/user/settings.php">
            <svg class="iconsvg" viewBox="0 0 24 24">
                <path d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
            </svg>
        </a>
    <?php
else:
?>
        <a href="/user/area/login.php" class="btn">Log In</a>
    <?php
endif;
?>
    </div>
</div>