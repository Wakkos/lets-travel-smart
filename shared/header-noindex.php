<?php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] = true) {
    header('location: user/dashboard.php');
}
?>
<!doctype html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
<meta name="robots" content="noindex">
<link rel="alternate" hreflang="en-us"
        href="https://flightmondo.com" />
<?php
  $analyticsPath = $_SERVER['DOCUMENT_ROOT'];
  $analyticsPath .= "/shared/analytics.php";
  include_once($analyticsPath);
?>
<meta charset="utf-8">

<title>Flightmondo: Flight deals for you</title>

<!-- http://bit.ly/18VB51x -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- CSS Para todos los navegadores -->
<link rel="stylesheet" href="/resources/css/style.css">




<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
</head>
