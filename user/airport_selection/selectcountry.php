<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once '../header.php';

?>

<?php
include_once "../../shared/topbar.php";
?>

<div class="breadcrumb">
    <a href="../dashboard.php" class="breadcrumb__item">Dashboard</a>
     <span class="breadcrumb__item">Select by Country</span>
</div>

<div class="stickyfooter">
    <h1 class="page-header">
        Choose a country
    </h1>
    <section class="block max-width">
        <div class="block__body padding">
                <?php
$sqlset = "SET GLOBAL group_concat_max_len=15000";
if ($result = mysqli_query($conn, $sqlset)) {

} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
}
$sqlTop5Countries = "SELECT co.country_name, co.country_code, count(*) AS cou FROM userairports ua
                    LEFT JOIN airports a ON a.airport_id = ua.airport_id
                    LEFT JOIN cities c ON c.city_id = a.city_id
                    LEFT JOIN countries co ON co.country_id = c.country_id
                    GROUP BY co.country_name
                    ORDER BY cou DESC
                    LIMIT 6";
$top5CountriesResult = mysqli_query($conn, $sqlTop5Countries);
$top5Countries = array();
if ($top5Countries !== false) {
    echo "<section class=\"flag-container\">";
    echo "<h4 class=\"grid--item-12\">Top countries</h4><hr class=\"grid--item-12\">";
    while ($row = mysqli_fetch_array($top5CountriesResult)) {
        $output = $row["country_code"];
        $countryName = $row["country_name"];
        $output = strtolower($output);

        echo "<a class=\"input-flag\" href=\"selectairport.php?country=$output\">
        <img src=\"/resources/img/flags/4x3/$output.svg\">$countryName
      </a>";
    }
    echo "</section>";
}

$name_continents = array();
$name_countries = array();
$sql = "SELECT con.continent_id,
                                con.continent_name,
                                cou.country_name,
                            GROUP_CONCAT('<a class=\"input-flag\" href=\"selectairport.php?country=',country_code,
                            '\"><img src=\"/resources/img/flags/4x3/',LOWER(country_code),'.svg\">',country_name,
                            '</a>'  ORDER BY country_name SEPARATOR '')
                        AS contents
                        FROM continents con
                        LEFT JOIN countries cou ON cou.continent_id = con.continent_id
                        GROUP BY con.continent_id";
if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) {
            echo "<section class=\"collapse__item nopadding\">";
                echo "<label for=\"cont-" . $row['continent_id'] . "\" class=\"collapse__title\">" . $row['continent_name'] . "</label>";
                echo "<input type=\"checkbox\" id=\"cont-" . $row['continent_id'] . "\" class=\"collapse__radio\">";
                echo "<div class=\"collapse__text collapse__text--flex flag-container\">";
                    echo $row['contents'];
                echo "</div>";
            echo "</section>";
        }
        // Free result set
        mysqli_free_result($result);
    } else {
        echo '<div class="alert--warning"> No records matching your query were found.</div>';
    }
} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
}

?>
            </div>
    </section>

</div>



<?php
include_once "../../shared/footer.php";
?>
 <script id="blockOfStuff" type="text/html">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24" class="airportBadge__icon iconsvg">
            <path d="M21,16V14L13,9V3.5A1.5,1.5 0 0,0 11.5,2A1.5,1.5 0 0,0 10,3.5V9L2,14V16L10,13.5V19L8,20.5V22L11.5,21L15,22V20.5L13,19V13.5L21,16Z" />
        </svg>
    </script>
    <script id="deleteicon"  type="text/html">
    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="#da4f49" d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />
</svg>
</script>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">




$(document).ready(function(){
        $("[role='tab']").click(function(){
            $("[role='tab']").attr("aria-selected","false"); //deselect all the tabs
            $(this).attr("aria-selected","true");  // select this tab
            var tabpanid= $(this).attr("aria-controls"); //find out what tab panel this tab controls
            var tabpan = $("#"+tabpanid);
            $("[role='tabpanel']").attr("aria-hidden","true").addClass("hidden"); //hide all the panels
            tabpan.attr("aria-hidden","false").removeClass("hidden");  // show our panel
            });
            // <!--This adds keyboard accessibility by adding the enter key to the basic click event.-->
            $("[role='tab']").keydown(function(ev) {
            if (ev.which ==13) {
            $(this).click();
            }
            });
            // <!--This adds keyboard function that pressing an arrow left or arrow right from the tabs toggel the tabs. -->
            $("[role='tab']").keydown(function(ev) {
            if ((ev.which ==39)||(ev.which ==37))  {
            var selected= $(this).attr("aria-selected");
            if  (selected =="true"){
            $("[aria-selected='false']").attr("aria-selected","true").focus() ;
            $(this).attr("aria-selected","false");
            var tabpanid= $("[aria-selected='true']").attr("aria-controls");
            var tabpan = $("#"+tabpanid);
            tabpan.attr("aria-hidden","false");
            tabpan.removeClass("hidden");
            }
            }
        });
    });
</script>
</body>
</html>