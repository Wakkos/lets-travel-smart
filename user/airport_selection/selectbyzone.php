

<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once '../header.php';



if (isset($_POST['iata'])) {
    $email = $_SESSION['email'];
    $user_id = $_SESSION['id'];
    $airportid = $_POST['iata'];
    $airporttomailchimp = false;

    $loginResult = "";

    if (isset($_POST['iata']) && is_array($_POST['iata'])) {
        foreach ($_POST['iata'] as $value) {
            $sql =
                "INSERT INTO userairports (airport_id, user_id)
                    SELECT a.airport_id, u.user_id
                    FROM users u
                    CROSS JOIN airports a
                    WHERE u.user_id = '$user_id'
                    AND a.airport_id = '$value'
                    AND not exists
                    (SELECT * FROM userairports ap where ap.airport_id = '$value' and ap.user_id = '$user_id')";
            if ($conn->query($sql) === true) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        $sql =
            "SELECT a.airport_iata, a.airport_id, ua.user_id
            FROM airports a
            INNER JOIN userairports ua
            ON a.airport_id = ua.airport_id
            WHERE ua.user_id='$user_id'";
        $results = mysqli_query($conn, $sql);
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {

                while ($row = mysqli_fetch_array($result)) {
                    $airporttomailchimp .= $row['airport_iata'] . ",";
                }
            } else {
                $loginResult .= '<div class="autocomplete__item alert--warning"> No records matching your query were found.</div>';
            }
        } else {
            $loginResult .= "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }

        include "../../helper/mailchimpvars.php";
        // member information

        $json = json_encode([
            'merge_fields' => [
                'AIRPORTS' => $airporttomailchimp,
            ],
        ]);
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo "This: " . $airporttomailchimp . "-";
        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = '<p style="color: #34A853">You have successfully updated.</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = 'Some problem occurred, please try again.';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">' . $msg . '</p>';
        }

        header('location: /user/dashboard.php');
    } else {
        echo '<div class="alert--warning grid--item-12">You have to select at least one departure airport.</div>';
    }
}
?>

<?php
include_once "../../shared/topbar.php";
?>

<div class="breadcrumb">
    <a href="selectcountry.php" class="breadcrumb__item">Select Country</a>
    <?php
        $country = $_GET["country"];
        echo "<a href='selectairport.php?country=".$country ."' class='breadcrumb__item'>Select Zone</a>";
    ?>

     <span class="breadcrumb__item">Select Airport</span>
</div>

<div class="stickyfooter">
<h1 class="page-header">
Choose your airports.
</h1>
    <section class="max-width block margin-bottom padding" id="js-hasAlert">
        <h2 class="block__title">We will send you offers from the airports you choose.</h2>
        <form action=""  method="post">
            <?php
$user_id = $_SESSION['id'];
$airportIdInUser[] = array();
if (isset($_GET['zone'])) {

    $zone = $_GET['zone'];
    echo "<div class=\"grid justify-spacebetween margin-bottom\">";
    $userAirports = "SELECT airports_usersairports from users_airports where users_usersairports = '$user_id'";
    if ($result = mysqli_query($conn, $userAirports)) {
        while ($row = mysqli_fetch_array($result)) {
            $airportIdInUser[] = $row['airports_usersairports'];
        }
    }
    $sql = "SELECT airport_id,airport_iata, airport_name, city_name FROM airports a
                        left join cities c on c.city_id = a.city_id
                        left join zones z on c.zone_id = z.zone_id
                        where z.zone_id ='$zone'
                        ";
    if ($result = mysqli_query($conn, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                if (in_array($row['airport_id'], $airportIdInUser)) {
                    $isChecked = "checked";
                } else {
                    $isChecked = null;
                }
                echo "<div class='grid--item-6 checkbox margin-bottom border-bottom'>
                                    <input type='checkbox' " . $isChecked . " class='js-checkbox'
                                    name='iata[]' value='" . $row['airport_id'] . "' id='" . $row['airport_iata'] . "'>
                                    <label for='" . $row['airport_iata'] . "' class='checkbox__label'>
                                    " . $row['city_name'] . " - " . $row['airport_name'] . "</label></div>";
            }

            // Free result set
            mysqli_free_result($result);
        } else {
            echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
        }
    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
    echo "</div>";

} else {
    echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
}
?>

            <div class="align-right">
                <input type="submit" class="btn" value="add airports" id="js-addAirports">
            </div>
        </form>

    </section>

</div>



<?php
include_once "../../shared/footer.php";
?>


<script type="text/javascript">


    function checkbox(){

        if (document.querySelector('.js-checkbox:checked')) {
            console.log('foo')
        } else {
                var tag = document.createElement("div");
                tag.setAttribute("class", "airportBadge");
                tag.setAttribute("id", "js-airportBadge");
                tag.setAttribute("onclick", "DeleteBadge(this);");
                document.getElementById("js-hasAlert").appendChild(tag);
                tag.innerHTML = '<span class="alert--error">you have to select at least one airport.</span>';
            return false;
        }
    }

</script>
</body>
</html>