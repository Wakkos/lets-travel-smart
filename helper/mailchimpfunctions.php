<?php

function Signup($email, $subscription)
{
    MailchimpBase($email, "subscribed", $subscription);
}

function Unsubscribe($email)
{
    MailchimpBase($email, "unsubscribed");
}

function MailchimpBase($email, $status, $subscription = null)
{
    include "./../helper/mailchimpvars.php";

    if (is_null($subscription)) {
        $json = json_encode([
            'email_address' => $email,
            'status' => $status,
            'merge_fields' => [
                'FNAME' => $email,
                'LNAME' => $email,
                'PREMIUM' => $subscription,
            ],
        ]);
    } else {
        $json = json_encode([
            'email_address' => $email,
            'status' => $status,
            'merge_fields' => [
                'FNAME' => $email,
                'LNAME' => $email,
            ],
        ]);
    }

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);
    $mchttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    echo "<script> console.log('$mchttpCode') </script>";

    if ($mchttpCode == 200) {
        $_SESSION['msg'] = '<p style="color: #34A853">You have successfully subscribed.</p>';
    } else {
        switch ($mchttpCode) {
            case 214:
                $_SESSION['msg'] = 'You are already subscribed.';
                break;
            default:
                $_SESSION['msg'] = 'Error adding to Mailchimp with email, not a 214.';
                break;
        }
        $_SESSION['msg'] = '<p style="color: #EA4335">' . $_SESSION['msg'] . '</p>';
    }

}
