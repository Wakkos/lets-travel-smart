SELECT continents.code_continents, continents.name_continents, country_zones, GROUP_CONCAT('<a class=\"input-flag ',alpha2_countries,'\">',name_zones,'</a>' SEPARATOR '') 
AS contents
            FROM continents
            
            LEFT JOIN countries ON countries.continentcode_countries = continents.code_continents
            left join zones ON country_zones = countries.name_countries
            where country_zones is not null
            GROUP BY continents.code_continents
            