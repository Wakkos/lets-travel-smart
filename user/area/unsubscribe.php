<?php
include_once "../../helper/session.php";
$userId = $_SESSION['id'];
include_once '../header.php';
include_once "../../shared/topbar.php";
$userCanUnsubscribe = RetrieveCurrentSubscription($userId);
?>
<div class="stickyfooter">

    <div class="breadcrumb block--medium">
        <a href="dashboard.php" class="breadcrumb__item">Dashboard</a>
        <a href="user/settings.php" class="breadcrumb__item">Settings</a>
        <span class="breadcrumb__item">Unsuscribe</span>
    </div>
    <main class="block block--payment centered-body">

        <h1 class="block__title">
            Subscription
        </h1>
        <div class="block__body">
<?php

if ($userCanUnsubscribe == true) {
    UnsubscribeInDB($userId);
} else {
    echo "This account has already unsubscribed.";
}

function RetrieveCurrentSubscription($userId)
{
    include "./../../phpscript/conn.php";
    $sql = "SELECT user_subscription, user_joindate, user_unsubscribed FROM users WHERE user_id = $userId";

    if ($result = mysqli_query($conn, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $userSubscription = $row['user_subscription'];
            $userMarkedToUnsubscribe = $row['user_unsubscribed'];
            $userJoinDate = $newformat = date('Y-m-d', strtotime($row['user_joindate']));
            if ($userSubscription <> 0 && $userMarkedToUnsubscribe <> 1) {
                $userUnsubscriptionDate = date('Y-m-d', strtotime("+3 months", strtotime($userJoinDate)));
                echo "$userUnsubscriptionDate";
                return true;
            }
        }
        mysqli_close($conn);
    }
    return false;
}

function UnsubscribeInDB($userId)
{
    include "./../../phpscript/conn.php";

    $updateSql = "UPDATE users SET user_unsubscribed = 1 WHERE user_id = $userId";
    if ($result = mysqli_query($conn, $updateSql)) {
        echo "Record was updated successfully.";
    } else {
        echo "ERROR: Could not able to execute $updateSql. "
                                . mysqli_error($conn);
    }
    mysqli_close($conn);
}

function UnsubscribeFromMailchimp()
{

}

function UnsubscribeFromStripe()
{

}
?>

        </div>
        <div class="block__footer">
            <a href="/user/settings.php" class="btn--secondary">Go back</a>
        </div>
    </main>
</div>