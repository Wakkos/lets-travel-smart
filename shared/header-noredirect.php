
<!doctype html>
<html lang="en">

<head>
<link rel="alternate" hreflang="en-us"
        href="https://flightmondo.com" />
        <?php
  $analyticsPath = $_SERVER['DOCUMENT_ROOT'];
  $analyticsPath .= "/shared/analytics.php";
  include_once($analyticsPath);
?>
<meta charset="utf-8">


<title>Flightmondo: Flight deals for you</title>

<!-- http://bit.ly/18VB51x -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- CSS Para todos los navegadores -->
<link rel="stylesheet" href="/resources/css/style.css">
<?php
  include_once "partials/facebook-meta.php";
?>



<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c5a8e9758b977001192591c&product='inline-share-buttons' async='async'></script>
</head>
