<?php
include_once "../helper/session.php";
include_once '../phpscript/conn.php';
include_once "stripe-init.php";
include_once 'header.php';
$userID = $_SESSION['id'];
?>

<?php
include_once "../shared/topbar.php";
?>
<div class="stickyfooter">
            <div class="alert max-width">
                <p class="nomargin-bottom">
                <b>This is a BETA version of FlightMondo.</b> If you experience issues or want us to improve any feature, <a href="/home/legal/flight-deals-contact.php">Let us know!</a>
                </p>
            </div>
            <div class="breadcrumb">
                <span class="breadcrumb__item">Dashboard</span>
            </div>
            <h1 class="page-header">
            Your departure airports:
        </h1>
            <?php
if (isset($_GET['signup'])) {
    ?>
                <div class="alert--success max-width">Welcome to your dashboard!</div>
                <?php
}
?>


    <main class="pagesection">

        <h2 class="block__title">We will send you offers from this airports only.</h2>

        <div class="block__body">
            <div class="margin-bottom grid">

            <?php

                $sql = "SELECT i.airport_id, a.airport_iata, a.airport_name, c.city_name
                FROM userairports i
                    inner join airports a on i.airport_id = a.airport_id
                    inner join cities c on a.city_id = c.city_id
                WHERE i.user_id = $userID";
                if ($result = mysqli_query($conn, $sql)) {
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            ?>
                            <div class="airportBadge">
                                <div class="airportBadge__header">
                                    <b><?php echo $row['airport_iata']?></b> - <?php echo $row['city_name']?>
                                </div>
                                <p class="airportBadge__description">
                                    <?php echo $row['airport_name']?>
                                </p>
                                <div class="airportBadge__actions">
                                    <button onclick='deleteField(this.id);' id="<?php echo $row['airport_id']?>" class='airportBadge__delete'>
                                    <svg viewBox="0 0 24 24" class="iconsvg">
                                        <path fill="#fff" d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />
                                    </svg>
                                        <span class='hide-sr'>Delete</span>
                                    </button>
                                </div>
                            </div>
                           <?php
                        }

                        mysqli_free_result($result);
                    } else {
                        ?>
                            <p class="color-lightgrey">You still have to pick airports! That's one of the main reasons you became premium, <a href="airport_selection/write-airport.php">go, choose!</a></p>
                        <?php
                    }
                } else {
                    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                }
                ?>
                </div>
                <h2 class="block__title">You can add more airports: </h2>
                <div class="btn-row">
                    <a href="airport_selection/selectcountry.php" class="btn btn--full">Add by country</a>
                    <a href="airport_selection/write-airport.php" class="btn btn--full">Search Airports</a>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "../shared/footer.php";
?>
<script>
    function deleteField(buttonID) {

    var airportID = buttonID;
    var email = '<?php echo $_SESSION['email']; ?>';
    var id = '<?php echo $_SESSION['id']; ?>';

    fetch('../helper/delete-field.php?airportID=' + airportID + '&email=' + email + '&id=' + id, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: "POST",
        body:  "airportID=" + airportID&"email=" + email
    })
    .then(function(response) {
        return response.text();
        }).then(function(data) {
            validate = data;
            if (data === "deleted") {
                var elem = document.getElementById(buttonID);
                elem.parentNode.parentNode.remove();
                return false;
                console.log(data);
            }
            else {
                console.log("Else: " + data);
            }
    })
    .catch(function(error) {
        console.log('Error: ' + error);
    });

}
</script>
</body>
</html>
