<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once '../header.php';

?>

<?php
include_once "../../shared/topbar.php";
?>
<div class="stickyfooter max-width">
<div class="breadcrumb">
    <a href="../dashboard.php" class="breadcrumb__item">Dashboard</a>
     <span class="breadcrumb__item">Select by Country</span>
</div>
    <h1 class="page-header">search for airports</h1>
    <section class="faux">
        <div class="faux__sidebar">
            <div class="block__title">
            What city or airport are you departing?
            </div>
            <?php
if (isset($_POST['iata'])) {
    $email = $_SESSION['email'];
    $user_id = $_SESSION['id'];
    $airportid = $_POST['iata'];
    $airporttomailchimp = false;
    $loginResult = "";
    if (isset($_POST['iata']) && is_array($_POST['iata'])) {
        foreach ($_POST['iata'] as $value) {
            $sql = "INSERT into userairports (airport_id, user_id)
                            SELECT a.airport_id, u.user_id
                            FROM users u
                            CROSS JOIN airports a
                            WHERE u.user_id ='$user_id'
                            AND a.airport_id = '$value'
                            AND not exists
                            (SELECT * FROM userairports ap where ap.airport_id = '$value' and ap.user_id = '$user_id')";
            if ($conn->query($sql) === true) {
                echo "<div class=\"alert--success grid--item-12\">New record created successfully</div>";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error . "" . $value . "";
            }
        }

        // Query to send to Mailchimp
        $sql = "SELECT a.airport_iata
                        FROM airports a
                        INNER JOIN userairports ua
                        ON a.airport_id=ua.airport_id
                        WHERE ua.user_id='$user_id'";
        $results = mysqli_query($conn, $sql);
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {

                while ($row = mysqli_fetch_array($result)) {
                    $airporttomailchimp .= $row['airport_iata'] . ",";
                }
            } else {
                $loginResult .= '<div class="autocomplete__item alert--warning"> No records matching your query were found.</div>';
            }
        } else {
            $loginResult .= "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }

        include "../../helper/mailchimpvars.php";
        // member information

        $json = json_encode([
            'merge_fields' => [
                'AIRPORTS' => $airporttomailchimp,
            ],
        ]);
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = '<p style="color: #34A853">You have successfully subscribed to CodexWorld.</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = 'Some problem occurred, please try again.';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">' . $msg . '</p>';
        }

        header('location: /user/dashboard.php');
    } else {
        echo '<div class="alert--warning grid--item-12">You have to select at least one departure airport.</div>';
    }
}
?>

            <div class="block__body padding-top">
                <label class="grid--item-12 margin-bottom" for="js-userlocations">Start typing your city or airport name/code</label>
                <div class="formgroup margin-bottom--mini">
                    <input type="text" size="50" id="js-userlocations" name="service" placeholder="NYC / New York" />
                    <span role="button" title="Add Airport" id="js-addLocation" class="btn--secondary">Add</span>
                </div>
                <div id="js-suggestions" class="autocomplete"></div>
            </div>
        </div>
        <div class="faux__main">
            <form method="post" action="" class="grid block__body">
                <div class="padding-top grid" id="js-locations"></div>
                <div class="grid--item-12 align-right">
                    <input type="submit" class="btn" value="Add and Update Airports" autocomplete="off">
                </div>
            </form>
        </div>
    </section>
</div>



<?php
include_once "../../shared/footer.php";
?>
 <script id="blockOfStuff" type="text/html">
        <svg viewBox="0 0 24 24" class="iconsvg">
            <path d="M21,16V14L13,9V3.5A1.5,1.5 0 0,0 11.5,2A1.5,1.5 0 0,0 10,3.5V9L2,14V16L10,13.5V19L8,20.5V22L11.5,21L15,22V20.5L13,19V13.5L21,16Z" />
        </svg>
    </script>
    <script id="deleteicon"  type="text/html">
    <svg viewBox="0 0 24 24" class="iconsvg">
    <path fill="#fff" d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />
</svg>
</script>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">

//Autocomplete function
$(document).ready(function() {
    //Al escribr dentro del input con id="service"
    $('#js-userlocations').on('input',function(){
        //Obtenemos el value del input
        var service = $(this).val();
        var dataString = 'service='+service;
        var userId = <?php echo $_SESSION['id']; ?>;

        //Le pasamos el valor del input al ajax
        $.ajax({
            type: "POST",
            url: "../autocomplete.php",
            data: {service: service, userId: userId},
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#js-suggestions').fadeIn(1000).html(data);
                $('#test').html(data);
                //Al hacer click en alguNa de las sugerencias
                $('#js-suggestions').on('click', '.suggest-element', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#js-userlocations').val(id);
                    //Hacemos desaparecer el resto de sugerencias
                    $('#js-suggestions').fadeOut(10);
                    //eLIMINAMOS EL VALUE DEL INPUT
                });
            }
        });
    });
});

//Add location function

window.onload = function() {
    document.getElementById("js-addLocation").onclick = function addLocation() {
        var locationValue = document.getElementById('js-userlocations').value;
        var stringtoSplit = locationValue;
        var splitstring = stringtoSplit.split(' - ');
        fetch('/airport-verification.php', {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: "POST",
            body:  "locationValue=" + splitstring[0]
        })
        .then(function(response) {
        return response.text();
        })
        .then(function(data) {

            if (data === "invalid") {
                console.log("No Existe, es " + data);
            }
            else {
                console.log("Existe, es " + data);
                var tag = document.createElement("div");
                var icon = document.getElementById('blockOfStuff').innerHTML;
                var deleteicon = document.getElementById('deleteicon').innerHTML;

                tag.setAttribute("class", "airportBadge");
                tag.setAttribute("id", splitstring[3]);
                document.getElementById("js-locations").appendChild(tag);
                tag.innerHTML = '<div class="airportBadge__header"><b>' + splitstring[2] + '</b> - ' + splitstring[1] + '                                </div><p class="airportBadge__description">name airport</p><div class="airportBadge__actions"><button onclick="deleteField(this.id);" id="' + splitstring[2] + '" class="airportBadge__delete">'+ icon +'<span class="hide-sr">Delete</span></button></div></div>';
                //  ' + splitstring[1] + '<span class="airportBadge__city--iata">' + splitstring[2] + '</span></span></div><div class="airportBadge__action"><span role="button" onclick="deleteField(this.id);" class="airportBadge__delete" id="' + splitstring[2] + '"><span class="hide-sr">Delete</span>' + deleteicon + '</span></div></div>';

                //  ' + splitstring[1] + '<span class="airportBadge__city--iata">' + splitstring[2] + '</span></span></div><div class="airportBadge__action"><span role="button" onclick="deleteField(this.id);" class="airportBadge__delete" id="' + splitstring[2] + '"><span class="hide-sr">Delete</span>' + deleteicon + '</span></div></div>';


                // Create hidden input
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", "iata[]");
                input.setAttribute("value", splitstring[3]);
                document.getElementById("js-locations").appendChild(input);

            }
        }).catch(function(error) {
            console.log('Error: ' + error);
        });
        // Clear the input
        document.getElementById("js-userlocations").value = "";
    }
}





function deleteField(buttonID) {
    var airportID = buttonID;
    var email = '<?php echo $_SESSION['email']; ?>';
    var id = '<?php echo $_SESSION['id']; ?>';
    var elem = document.getElementById(buttonID);
    elem.parentNode.parentNode.remove();
    return false;
}
</script>
</body>
</html>