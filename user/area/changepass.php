<?php

include_once "../../phpscript/conn.php";

include_once "../../shared/header-noindex.php";
?>
<body class="dashboard">

<?php

include_once "../../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
        <h1 class="block__title align-center">Recover your password.</h1>
        <div class="block__body">

        <?php
if (isset($_GET["email"]) && isset($_GET["token"]) && !isset($_POST["newpass"])) {

    $email = $_GET["email"];
    $token = $_GET["token"];
    $sql = "SELECT user_email, user_resetpasstocken, user_resetpasstime  FROM users
            WHERE user_email='$email' AND user_resetpasstocken='$token'";
    $results = mysqli_query($conn, $sql);
    // If email adn token exist:
    if ($result = mysqli_query($conn, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                // We get the time the token was created, if it has more than 24h, we invalidate it.
                $mysqltime = DateTime::createFromFormat("Y-m-d H:i:s", $row['user_resetpasstime']);
                $currentdatetime = new DateTime();
                $interval = $mysqltime->diff($currentdatetime);
                $interval->format('%h hours %i minutes');

                $hours = $interval->h;
                $hours = $hours + ($interval->days * 24);
                if ($hours > 24) {
                    echo "<p> Sorry, seems like that link expired.</p>";
                } else {
                    ?>
                            <form action="" method="post" onsubmit="return comparepass()">
                                <label for="">New password</label>
                                <input type="password" id="pass" class="margin-bottom" name="newpassword" minlength="8" required placeholder="**********">
                                <label for="">Confirm new password</label>
                                <input type="password" id="passconf" class="margin-bottom" name="conmfirmpassword" minlength="8" required placeholder="**********">
                                <div id="alert"></div>
                                <div class="align-right">
                                    <input type="submit" name="newpass" class="btn" value="submit">
                                </div>
                            </form>
                        <?php
}
            }
        } else {
            echo "<p class=\"alert--error\">Looks like that link is invalid. =(</p>";
        }
    }

} else if (isset($_POST["newpass"]) && isset($_GET["email"]) && isset($_GET["token"])) {
    $password = $_POST['newpassword'];
    $email = $_GET["email"];

    if (8 > strlen($password)) {
        echo "<p class=\"alert--error\"> Sorry, your password cannot be shorter than 8 characters.</p>";
    } else {
        $hased_pass = password_hash($password, PASSWORD_DEFAULT);
        $sql = "UPDATE users SET user_resetpasstocken = NULL,
                 users_resetpasstime = NULL, user_password = '$hased_pass' WHERE user_email='$email'";
        $results = mysqli_query($conn, $sql);

        echo "<p class=\"alert--success\">Your password has been updated. You can <a href=\"/login.php\">log in now</a></p>";

    }
} else {
    echo "<p>Looks like you come from an invalid link. Do you still need to <a href=\"resetpass.php\">recover your password?</a>";

}
?>

	</section>
</div>
<?php
include_once "../../shared/footer.php";
?>
<script>
function comparepass() {
    var pass = document.getElementById("pass");
    var passconf = document.getElementById("passconf");

    if(pass.value !== passconf.value) {
        var alertmsg = document.createElement("div");
        alertmsg.setAttribute("class", "alert--error");
        document.getElementById("alert").appendChild(alertmsg);
        alertmsg.innerHTML = 'Passwords do not match';
        return false;
    }
    else {
        return true; // submit the form
    }
}
</script>
</body>
</html>

