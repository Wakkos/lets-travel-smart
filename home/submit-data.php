<?php
include_once '../phpscript/conn.php';
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">

<title>Cheap flights</title>


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<link rel="stylesheet" href="css/style.css">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=PT+Sans|PT+Serif" rel="stylesheet">
</head>
<script src="js/zxcvbn.js"></script>
<body>
<?php

if (isset($_GET["email"])) {

    $user = $_GET["email"];

} else {
    $user = null;
    echo "no email supplied";
}
?>
    <section class="block block--small block--white">
        <?php
if (isset($_POST["finalEmail"])) {
    $email = $_POST["finalEmail"];
    $password = $_POST["password"];
    $hased_pass = password_hash($password, PASSWORD_DEFAULT);
    $airport = $_POST["airport"];
    echo "<script>console.log('yeyyyy')</script>";
    $sql = "INSERT INTO users (user_email, user_password, user_joindate) VALUES ('$email', '$hased_pass', NOW())";

    if ($conn->query($sql) === true) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>
        <h1 class="block__title">We need some quick input from you</h1>
        <div class="block__body">
            <form method="post" action="">
                <label for="Password">Your email</label>
                <input type="email" class="margin-bottom" name="finalEmail" placeholder="name@domain.com">

                <label for="Password">Create a Password</label>
                <input type="password" name="password" id="password" required placeholder="password">
                <meter max="4" id="password-strength-meter"></meter>
                <p id="password-strength-text"></p>

                <label for="service">What city or airport are you departing?</label>
                <small>We will look departures from that airport!</small>
                <div class="formgroup margin-bottom--mini">
                    <input type="text" size="50" id="js-userlocations" name="service" />
                    <span role="button" title="Add Airport" id="js-addLocation" class="btn">Add</span>
                </div>
                <div id="js-suggestions" class="autocomplete"></div>
                <div class="padding-top" id="js-locations"></div>
                <input type="submit" class="btn--cta" value="Send me cheap flights!">
            </form>
            <div class="alert--block">
                <div class="alert--block__icon">
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24" class="iconsvg">
                        <path d="M13,14H11V10H13M13,18H11V16H13M1,21H23L12,2L1,21Z" />
                    </svg>
                </div>
                <p>You can only add one departure airport. You can many airports as you want than one airport with our Premium subscription.</p>
                <div class="align-center">
                    <a href="#" class="btn--cta btn--small">Learn more</a>
                </div>
            </div>
        </div>
    </section>
    <script id="blockOfStuff" type="text/html">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24" class="airportBadge__icon iconsvg">
            <path d="M21,16V14L13,9V3.5A1.5,1.5 0 0,0 11.5,2A1.5,1.5 0 0,0 10,3.5V9L2,14V16L10,13.5V19L8,20.5V22L11.5,21L15,22V20.5L13,19V13.5L21,16Z" />
        </svg>
    </script>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">

//Autocomplete function
$(document).ready(function() {
    //Al escribr dentro del input con id="service"
    $('#js-userlocations').on('input',function(){
        //Obtenemos el value del input
        var service = $(this).val();
        var dataString = 'service='+service;

        //Le pasamos el valor del input al ajax
        $.ajax({
            type: "POST",
            url: "autocomplete.php",
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#js-suggestions').fadeIn(1000).html(data);
                $('#test').html(data);
                //Al hacer click en alguNa de las sugerencias
                $('#js-suggestions').on('click', '.suggest-element', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#js-userlocations').val(id);
                    //Hacemos desaparecer el resto de sugerencias
                    $('#js-suggestions').fadeOut(1000);
                    //eLIMINAMOS EL VALUE DEL INPUT
                });
            }
        });
    });
});

//Add location function

window.onload = function() {
    document.getElementById("js-addLocation").onclick = function addLocation() {
        var badgeExists = document.getElementById("js-airportBadge");

                if (badgeExists != null) {
                    alert("You cannot add more");
                    return;
                }
        var locationValue = document.getElementById('js-userlocations').value;
        var stringtoSplit = locationValue;
        var splitstring = stringtoSplit.split(' - ');
        fetch('airport-verification.php', {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: "POST",
            body:  "locationValue=" + splitstring[0]
        })
        .then(function(response) {
        return response.text();
        })
        .then(function(data) {

            if (data === "invalid") {
                console.log("No Existe, es " + data);
            }
            else {
                console.log("Existe, es " + data);
                var tag = document.createElement("div");
                var icon = document.getElementById('blockOfStuff').innerHTML;

                tag.setAttribute("class", "airportBadge");
                tag.setAttribute("id", "js-airportBadge");
                tag.setAttribute("onclick", "DeleteBadge(this);");
                document.getElementById("js-locations").appendChild(tag);
                tag.innerHTML = splitstring[0] + '<span class="airportBadge__city">' + splitstring[1] + '</span>' + icon;


                // Create hidden input
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", "airport");
                input.setAttribute("value", locationValue);
                document.getElementById("js-locations").appendChild(input);
                document.getElementById("js-addLocation").className = "btn--disabled";
                document.getElementById("js-addLocation").title = "You can add only one with our free account";
            }
        }).catch(function(error) {
            console.log('Error: ' + error);
        });
        // Clear the input
        document.getElementById("js-userlocations").value = "";
    }

}


// Pass Strength
var strength = {
  0: "Worst",
  1: "Bad",
  2: "Weak",
  3: "Good",
  4: "Strong"
};
var password = document.getElementById('password');
var meter = document.getElementById('password-strength-meter');
var text = document.getElementById('password-strength-text');

password.addEventListener('input', function() {
  var val = password.value;
  var result = zxcvbn(val);

  // Update the password strength meter
  meter.value = result.score;

  // Update the text indicator
  if (val !== "") {
    text.innerHTML = "Strength: " + strength[result.score];
  } else {
    text.innerHTML = "";
  }
});

// This delete the Airport Badge
function DeleteBadge(o) {
    var p=o.closest("#js-airportBadge");
    p.parentNode.removeChild(p);
    document.getElementById("js-addLocation").className = "btn";
    document.getElementById("js-addLocation").title = "You can add only one with our free account";
}
</script>
</body>
</html>