<?php

include_once "../phpscript/conn.php";
include_once "../user/stripe-init.php";
include_once "../shared/header.php";
?>


<body class="dashboard">
<?php
include_once "../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
		<h1 class="block__title align-center">Sign Up for flight deals.</h1>
		<form method="post" action="welcome.php" onsubmit="return valEmail()" class="stripe" id="js-emailform">
			<div class="block__body">
				<!-- <div id="fielderrors"></div>
				<legend>Choose your plan:</legend>
				<div class="grid margin-bottom has-separator">
					<div class="checkbox grid--item-6">
						<input type="radio" id="anual" name="suscription" value="1" checked="checked">
						<label class="checkbox__label" for="anual"><b>Annual suscription:</b> <small class="display-block">$20 billed every year </small></label>
					</div>
					<div class="checkbox grid--item-6">
						<input type="radio" id="trimester" name="suscription" value="2">
						<label class="checkbox__label" for="trimester"><b>3 month suscription:</b><small class="display-block"> $10 billed every threee months </small></label>
					</div>
					<div class="checkbox grid--item-6">
						<input type="radio" id="trimester" name="suscription" value="3">
						<label class="checkbox__label" for="trimester"><b>free subscription:</b><small class="display-block"> free account </small></label>
					</div>
				</div> -->
				<div>
				<p>This is a beta trial expected to last till 01/04/2019. Till then enjoy all the benefits of a premium account for <b>free</b>!</p>
				<p>If you encounter any errors please let us know using the <a href="/home/legal/flight-deals-contact.php">Contact Us</a> section</p>
				</div>

				<label for="Password">Email</label>
				<input type="email" class="margin-bottom-xl" name="finalEmail" required placeholder="janedoe@gmail.com" id="email">
				<label for="Password">Create a Password</label>
				<div class="formgroup margin-bottom-xl">
					<input type="password" name="password" required id="password" placeholder="Must be at least 6 characters" class="nomargin-bottom">
					<meter max="4" id="password-strength-meter"></meter>
					<span id="password-strength-text" class="iconholder meter-icon"></span>
				</div>
				<input type="checkbox" required name="terms"> I accept the <a href="https://privacypolicies.com/privacy/view/0197e64b179431ac0ff921a99bed60bc" target="_blank">Terms and Conditions</a>
				<!-- <label>Credit Card details</label>

				<div id="card-element"> -->
				<!-- A Stripe Element will be inserted here. -->
				<!-- </div> -->
				<!-- Used to display Element errors. -->
				<!-- <div id="card-errors" role="alert"></div>
				<div class="ccicons margin-bottom">
					<img src="/resources/img/visa.png" alt="Get flight deals with Visa">
					<img src="/resources/img/amex.png" alt="Get flight deals with American Express">
					<img src="/resources/img/mastercard.png" alt="Get flight deals with Master Card">

				</div> -->
				<!-- <div class="grid">
					<div class="grid--item-4 flex-right">
						<label for="coupon_id"><small>Have a discount code?</small></label>
						<input type="text" name="coupon_id" id="coupon_id">
					</div>
				</div> -->
			</div>
			<div class="block__footer">
			<a href="/" class="btn--secondary margin-bottom">Cancel</a>
				<button id="js-btnsubmit" class="btn margin-bottom">Sign Up</button>
				<p class="align-right font-small nomargin-bottom">
					Already have an account? <a href="/user/area/login.php">Log in.</a>
				</p>
			</div>
		</form>
	</section>
</div>
<?php
include_once "../shared/footer.php";
?>
<script src="https://js.stripe.com/v3/"></script>
<script id="thief" type="text/html">
<svg version="1.1" class="iconsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 295.996 295.996" style="enable-background:new 0 0 295.996 295.996;" xml:space="preserve">
<g>
	<path d="M147.998,0C66.392,0,0,66.392,0,147.998c0,81.606,66.392,147.998,147.998,147.998c81.606,0,147.998-66.392,147.998-147.998
		C295.996,66.392,229.605,0,147.998,0z M147.998,279.996c-36.257,0-69.143-14.696-93.023-38.44
		c-9.536-9.482-17.631-20.41-23.934-32.42c-4.641-8.842-8.315-18.266-10.866-28.139H275.82
		C261.13,237.865,209.392,279.996,147.998,279.996z M25.037,99.997C44.278,50.877,92.14,16,147.998,16
		c34.523,0,65.987,13.328,89.533,35.102c12.208,11.288,22.289,24.844,29.558,39.997c1.395,2.908,2.686,5.877,3.87,8.898H25.037z
		 M213.497,140.497c0,8.836-7.164,16-16,16c-8.836,0-16-7.164-16-16c0-8.836,7.164-16,16-16
		C206.333,124.497,213.497,131.661,213.497,140.497z M114.497,140.497c0,8.836-7.164,16-16,16c-8.836,0-16-7.164-16-16
		c0-8.836,7.164-16,16-16C107.333,124.497,114.497,131.661,114.497,140.497z"/>

		<rect x="70.767" y="69.685" transform="matrix(-0.9689 -0.2473 0.2473 -0.9689 174.7187 177.3174)" width="55.46" height="15.999"/>

		<rect x="189.498" y="49.954" transform="matrix(-0.2473 -0.9689 0.9689 -0.2473 171.0742 288.259)" width="15.999" height="55.46"/>
	<path d="M200.159,205.505c12.875-1.788,26.044,1.092,37.084,8.104l8.578-13.506c-14.238-9.043-31.233-12.752-47.862-10.445
		c-26.732,3.714-49.436,22.684-57.84,48.329l15.205,4.982C161.838,223.089,179.438,208.384,200.159,205.505z"/>
</g>

</svg>

</script>
<script id="embarrased" type="text/html">
<svg version="1.1" class="iconsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 296.256 296.256" style="enable-background:new 0 0 296.256 296.256;" xml:space="preserve">
<g>
	<path d="M98.497,147.128c4.39,0,8.364-1.771,11.255-4.634c2.929-2.899,4.745-6.92,4.745-11.366c0-8.836-7.164-16-16-16
		s-16,7.164-16,16c0,2.079,0.409,4.059,1.131,5.882C85.974,142.933,91.74,147.128,98.497,147.128z"/>
	<path d="M212.366,137.01c0.722-1.822,1.131-3.803,1.131-5.882c0-8.836-7.164-16-16-16c-8.836,0-16,7.164-16,16
		c0,4.446,1.816,8.467,4.745,11.367c2.891,2.862,6.865,4.633,11.255,4.633C204.254,147.128,210.02,142.933,212.366,137.01z"/>
	<rect x="114.664" y="206.127" width="66" height="16"/>
	<path d="M296.256,76.269c0-9.527-5.948-21.439-18.184-36.416c-8.415-10.3-16.722-18.2-17.07-18.531l-5.504-5.217l-5.503,5.216
		c-0.254,0.24-4.698,4.471-10.327,10.7c-25.226-19.959-57.08-31.891-91.67-31.891C66.392,0.13,0,66.521,0,148.128
		c0,81.606,66.392,147.998,147.998,147.998s147.998-66.392,147.998-147.998c0-16.614-2.753-32.599-7.825-47.52
		C293.247,93.812,296.256,85.385,296.256,76.269z M279.996,148.128c0,72.784-59.214,131.998-131.998,131.998
		c-36.256,0-69.143-14.696-93.022-38.44c-9.536-9.482-17.631-20.41-23.934-32.42C21.442,190.977,16,170.178,16,148.128
		C16,75.344,75.214,16.13,147.998,16.13c30.694,0,58.975,10.531,81.419,28.169c-0.454,0.596-0.9,1.187-1.333,1.771
		c-8.959,12.102-13.346,22.03-13.346,30.198c0,22.475,18.285,40.76,40.76,40.76c5.347,0,10.451-1.046,15.134-2.925
		c1.496-0.602,2.952-1.279,4.354-2.047C278.249,123.526,279.996,135.627,279.996,148.128z M255.498,101.028
		c-13.652,0-24.76-11.107-24.76-24.76c0-8.673,13.101-25.674,24.763-37.847c11.659,12.161,24.755,29.153,24.755,37.847
		C280.256,89.921,269.149,101.028,255.498,101.028z"/>
</g>
</svg>

</script>
<script id="indiferent" type="text/html">
<svg version="1.1" class="iconsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 295.996 295.996" style="enable-background:new 0 0 295.996 295.996;" xml:space="preserve">
<g>
	<path d="M147.998,0C66.392,0,0,66.392,0,147.998c0,81.606,66.392,147.998,147.998,147.998c81.606,0,147.998-66.392,147.998-147.998
		C295.996,66.392,229.604,0,147.998,0z M147.998,279.996c-36.257,0-69.143-14.696-93.023-38.44
		c-9.536-9.482-17.631-20.41-23.934-32.42C21.442,190.847,16,170.048,16,147.998C16,75.214,75.214,16,147.998,16
		c34.523,0,65.987,13.328,89.533,35.102c12.208,11.288,22.289,24.844,29.558,39.997c8.27,17.238,12.907,36.537,12.907,56.899
		C279.996,220.782,220.782,279.996,147.998,279.996z"/>
	<path d="M176.249,181.991c12.875-1.786,26.044,1.091,37.084,8.104l8.578-13.506c-14.238-9.043-31.234-12.751-47.862-10.445
		c-26.732,3.714-49.436,22.684-57.84,48.329l15.205,4.982C137.928,199.575,155.527,184.87,176.249,181.991z"/>
	<circle cx="98.665" cy="115.998" r="16"/>
	<circle cx="197.665" cy="115.998" r="16"/>
</g>

</svg>
</script>
<script id="happiness" type="text/html">
<svg version="1.1" class="iconsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 295.996 295.996" style="enable-background:new 0 0 295.996 295.996;" xml:space="preserve">
<g>
	<path d="M147.998,0C66.392,0,0,66.392,0,147.998c0,81.606,66.392,147.998,147.998,147.998c81.606,0,147.998-66.392,147.998-147.998
		C295.996,66.392,229.605,0,147.998,0z M147.998,279.996c-36.257,0-69.143-14.696-93.023-38.44
		c-9.536-9.482-17.631-20.41-23.934-32.42C21.442,190.847,16,170.047,16,147.998C16,75.214,75.214,16,147.998,16
		c34.523,0,65.987,13.328,89.533,35.102c12.208,11.288,22.289,24.844,29.558,39.996c8.27,17.239,12.907,36.538,12.907,56.9
		C279.996,220.782,220.782,279.996,147.998,279.996z"/>
	<path d="M97.41,114.4c8.6,0,15.597,6.597,15.597,15.597h16c0-18-14.174-31.597-31.597-31.597
		c-17.423,0-31.597,13.597-31.597,31.597h16C81.813,120.997,88.811,114.4,97.41,114.4z"/>
	<path d="M198.584,114.4c8.6,0,15.597,6.597,15.597,15.597h16c0-18-14.174-31.597-31.597-31.597
		c-17.423,0-31.597,13.597-31.597,31.597h16C182.987,120.997,189.984,114.4,198.584,114.4z"/>
	<path d="M147.715,229.995c30.954,0,60.619-15.83,77.604-42.113l-13.439-8.684c-15.596,24.135-44.134,37.605-72.693,34.308
		c-22.262-2.567-42.849-15.393-55.072-34.308l-13.438,8.684c14.79,22.889,39.716,38.409,66.676,41.518
		C140.814,229.8,144.27,229.995,147.715,229.995z"/>
</svg>
</script>
<script src="/resources/js/signup.js"></script>
<script src="/resources/js/zxcvbn.js"></script>
<!-- <script type="text/javascript">
var stripe = Stripe('pk_test_I2S1nIeZ3srPaU0qC4m7PNvn');
var elements = stripe.elements();


// Create an instance of the card Element.
var card = elements.create('card', {
	iconStyle: 'solid',
    style: {
      base: {
        iconColor: 'black',
        color: '#586368',
        fontWeight: 500,
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',

        ':-webkit-autofill': {
          color: '#fce883',
        },
        '::placeholder': {
          color: '#bfbfbf',
        },
      },
      invalid: {
        iconColor: 'rgb(218,79,73)',
        color: 'rgb(218,79,73)',
      },
    },
});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');


card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
    displayError.className += " alert--error";
  } else {
    displayError.textContent = '';
    displayError.classList.remove("alert--error");
  }
});


// Create a token or display an error when the form is submitted.
var form = document.getElementById('js-emailform');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('js-emailform');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}






//Autocomplete function
var thief = document.getElementById('thief').innerHTML;
var embarrased = document.getElementById('embarrased').innerHTML;
var happiness = document.getElementById('happiness').innerHTML;
var indiferent = document.getElementById('indiferent').innerHTML;

// Pass Strength
var strength = {
  0: thief,
  1: thief,
  2: embarrased,
  3: indiferent,
  4: happiness
};
var password = document.getElementById('password');
var meter = document.getElementById('password-strength-meter');
var text = document.getElementById('password-strength-text');

password.addEventListener('input', function() {
  var val = password.value;
  var result = zxcvbn(val);

  // Update the password strength meter
  meter.value = result.score;

  // Update the text indicator
  if (val !== "") {
    text.innerHTML = strength[result.score];
  } else {
    text.innerHTML = "";
  }
});
</script> -->
</body>
</html>