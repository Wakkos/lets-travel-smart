<?php
include_once '../phpscript/conn.php';

header('Content-Type: text/plain');

$search = $_POST['service'];
$userId = $_POST['userId'];
$userIdArray = array();
$airportIdArray[] = array();

$exists = "SELECT airport_id
            FROM usersairports
            WHERE user_id = $userId";
$result = mysqli_query($conn, $exists);
if ($result === true) {
    while ($row = mysqli_fetch_array($result)) {
        $airportIdArray[] = $row['airport_id'];
    }
}
$sql = "SELECT distinct a.airport_iata, c.city_name, a.airport_name, a.airport_id FROM airports a, cities c
WHERE (INSTR(c.city_name,'". $search ."') OR INSTR(a.airport_name,'" . $search . "')  
OR INSTR(a.airport_iata,'" . $search . "'))  AND a.city_id = c.city_id ORDER BY airport_name DESC";
if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {

        while ($row = mysqli_fetch_array($result)) {
            //Comparar Array con ID de resultado y asigno valor de variable
            if (in_array($row['airport_id'], $airportIdArray)) {
                $alreadyAdded = 'autocomplete__item--added';
            } else {
                $alreadyAdded = 'suggest-element';
            }
            echo '<div data-id="' . $row['airport_id'] . '" id="' . $row['airport_name'] . ' - ' . $row['city_name'] . ' - ' . $row['airport_iata'] . ' - ' . $row['airport_id'] . '" class="' . $alreadyAdded . ' autocomplete__item">';
            echo '<div><b>' . $row['airport_name'] . '</b> (' . $row['airport_iata'] . ')</div><small class="display-block">' . $row['city_name'] . '</small>';
            echo "</div>";
        }

        // Free result set
        mysqli_free_result($result);
    } else {
        echo '<div class="autocomplete__item alert--warning"> No records matching your query were found.</div>';
    }
} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
}

// Close connection
mysqli_close($conn);
