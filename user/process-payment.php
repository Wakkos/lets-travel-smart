<?php
require_once 'stripe-init.php';
include_once "../phpscript/conn.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = false;
    try {
        if (isset($_POST['customer_id'])) {
            $charge = \Stripe\Charge::create(array(
                'customer' => $_POST['customer_id'],
                'amount' => 53500,
                'currency' => 'usd',
                'description' => 'Single quote purchase after login'));
        } else if (isset($_POST['stripeToken'])) {
            // Check on the email address exists

            $customer = \Stripe\Customer::create(array(
                'source' => $_POST['stripeToken'],
                'email' => $_POST['finalEmail'],
                'plan' => 'plan_CgD2IYS4eO3pmS',
            ));

            //Create customer iun DB
            if (isset($_POST["finalEmail"])) {
                $email = $_POST["finalEmail"];
                $password = $_POST["password"];

                if ("" == trim($email) && "" == trim($password)) {
                    $result .= "<div class='alert--error font-small'>Email or password can't be blank</div>";
                } else {
                    $hased_pass = password_hash($password, PASSWORD_DEFAULT);
                    $sql = "INSERT INTO users (email, password, join_date) VALUES ('$email', '$hased_pass', NOW() )";

                    if ($conn->query($sql) === true) {
                        $result = "Registration Successful!";

                    } else {
                        $result = "Error: " . $sql . "<br>" . $conn->error;
                    }
                }

            }
        } else {
            throw new Exception("The Stripe Token or customer was not generated correctly");
        }
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
    if (!$error) {
        echo "<h2>Thank you for signing up! You'll be getting your Wilde quotes daily in your e-mail</h2>";
    } else {
        echo "<div class=\"alert--error\">" . $error . "</div><a class=btn--secondary' href='signup.php'>Go Back</a>";

    }
}
