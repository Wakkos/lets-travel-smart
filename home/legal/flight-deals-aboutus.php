<?php
include_once "../../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey">
<?php
include_once "../../helper/session.php";
include_once "../../shared/topbar.php";
?>

<section class="max-width padding-section stickyfooter">
    <h2 class="align-center margin-bottom-xl grid--item-12 ">About Us</h2>
    <p>Flight Mondo is a Canadian based company with clients & team members all over the globe. Our aim is to become the GLOBAL HUB of flight deals and travel advising.</p>
    At FlightMondo, we are dedicated to solving one of the biggest problems with air travel planning: too much information. We use the latest technology to select, filter and display only the information relevant to your chosen airports and send it directly to your email.
    <p>At <b>FlightMondo</b>, we are dedicated to solving one of the biggest problems with air travel planning: too much information. We use the latest technology to select, filter and display only the information relevant to your chosen airports and send it directly to your email.
</p>


<h3>We</h3>
<ul>
    <li>Are based in Canada. Eh!</li>
    <li>Like to travel a lot.</li>
    <li>Are professional web developers with many years of experience.</li>
</ul>
<p><strong>Join us as a <a href="/home/flight-deals-optin.php">FREE</a> or <a href="/home/signup.php">PREMIUM</a> member. Start flying to the places of your dreams for 50% off and more.
</strong></p>
</section>

<?php
include_once "../../shared/footer.php";
?>

</body>
</html>
