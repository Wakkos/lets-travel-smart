ALTER TABLE `momondo2`.`cities` 
CHARACTER SET = utf8mb4 , 
CHANGE COLUMN `city_name` `city_name` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ;
ALTER TABLE `momondo2`.`adminusers` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `adminuser_username` `adminuser_username` VARCHAR(30) CHARACTER SET 'utf8mb4' NOT NULL ;
ALTER TABLE `momondo2`.`airports` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `airport_name` `airport_name` VARCHAR(60) CHARACTER SET 'utf8mb4' NOT NULL ;
ALTER TABLE `momondo2`.`continents` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `continent_name` `continent_name` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ;
ALTER TABLE `momondo2`.`countries` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `country_name` `country_name` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ;
ALTER TABLE `momondo2`.`users` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `user_email` `user_email` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ,
CHANGE COLUMN `user_password` `user_password` VARCHAR(90) CHARACTER SET 'utf8mb4' NOT NULL ,
CHANGE COLUMN `user_stripeid` `user_stripeid` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ,
CHANGE COLUMN `user_resetpasstocken` `user_resetpasstocken` VARCHAR(45) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL ;
ALTER TABLE `momondo2`.`zones` 
CHARACTER SET = utf8mb4 ,
CHANGE COLUMN `zone_name` `zone_name` VARCHAR(45) CHARACTER SET 'utf8mb4' NOT NULL ;