<?php
include_once "helper/session.php";
include_once 'phpscript/conn.php';
include_once "shared/header.php";
?>
<body itemscope itemtype="https://schema.org/Service">
<?php
include_once "shared/topbar.php";
?>
<header class="header--home">
    <div class="header--home__texts">
        <h1>Airline tickets deals in your email!</h1>
        <p><strong>We will save you hundreds of dollars on your next flight(s).</strong></p>
        <p><span>Sign up and get emails with unique deals from the departure cities you select to a wide range of airports worldwide.</span></p>
    </div>
    <div class="header--home__cta">
        <section class="block block--white margin-bottom">
            <h2 class="block__title">Join us</h2>
            <div class="block__body">

                <ul class="checklist margin-bottom-xl">
                    <li class="checklist__item">Receive offers from your preferred airports.</li>
                    <li class="checklist__item">Choose as many departure airports as you need.</li>
                    <!-- <li class="checklist__item">Get access to better deals than with a free account.</li>
                    <li class="checklist__item">Only $20 a year <small>(Or less, see our plans)</small></li> -->
                </ul>
                <a href="/home/signup.php" class="btn--cta btn--full margin-bottom-xl">Sign me up!</a>
                <!-- <p>You can still receive good deals by opting in (just not the best available). Let us know your email and we will send you occasional deals.</p>
                <a href="/home/flight-deals-optin.php" class="btn btn--full">Opt In</a> -->
            </div>
        </section>
    </div>
</header>
<section class="max-width grid justify-spacearound padding-section">
    <h2 class="align-center margin-bottom-xl grid--item-12">How does it work?</h2>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="140 934.343 49.093 34.8"><defs></defs><path class="a" d="M19.4,5a8.7,8.7,0,1,1-8.7,8.7A8.7,8.7,0,0,1,19.4,5m0,21.75c9.62,0,17.4,3.9,17.4,8.7V39.8H2V35.45c0-4.8,7.78-8.7,17.4-8.7m19.886-2.709L32.45,16.583,35.333,13.7l3.952,3.952,8.924-8.924,2.883,3.5Z" transform="translate(138 929.343)"/></svg>
        <p>You sign up to receive the best available deals.</p>
    </div>
    <div class="arrow">
        <div class="arrow__item"></div>
    </div>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="968 1071.371 50.41 43.326"><defs><style>.a{fill:rgba(0,0,0,0.7);}</style></defs><path class="a" d="M3.481,41.925H50.709V46.9H3.481V41.925M52.126,18.658a3.72,3.72,0,0,0-4.574-2.635l-13.2,3.53L17.152,3.57,12.4,4.838,22.7,22.66,10.341,25.966l-4.9-3.828-3.6.969,4.524,7.855,1.914,3.306,3.977-1.044,13.2-3.555,10.813-2.883,13.2-3.5A3.821,3.821,0,0,0,52.126,18.658Z" transform="translate(966.16 1067.802)"/></svg>
        <p>Choose your preferred departure airports.</p>
    </div>
    <div class="arrow">
        <div class="arrow__item"></div>
    </div>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="968 933.331 42.905 38.239"><defs></defs><path class="a" d="M27.709,19.877A8.052,8.052,0,0,1,34.52,32.212l5.506,5.542-2.485,2.485-5.578-5.488a8.045,8.045,0,1,1-4.255-14.874m0,3.575a4.469,4.469,0,1,0,4.469,4.469,4.469,4.469,0,0,0-4.469-4.469M34.592,12.78a8.916,8.916,0,0,1,4.737,16.036v-.894a11.62,11.62,0,1,0-23.24,0,12.461,12.461,0,0,0,.3,2.682H10.726A10.731,10.731,0,0,1,9.564,9.2,13.4,13.4,0,0,1,34.592,12.78Z" transform="translate(968 931.331)"/></svg>
        <p><span itemprop="brand">FlightMondo</span> looks for the best deals EVERY WEEK.</p>
    </div>
    <div class="arrow">
        <div class="arrow__item"></div>
    </div>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="140 1079.343 43.5 34.8"><defs></defs><path class="a" d="M41.15,12.7,23.75,23.575,6.35,12.7V8.35l17.4,10.875L41.15,8.35m0-4.35H6.35A4.335,4.335,0,0,0,2,8.35v26.1A4.35,4.35,0,0,0,6.35,38.8h34.8a4.35,4.35,0,0,0,4.35-4.35V8.35A4.349,4.349,0,0,0,41.15,4Z" transform="translate(138 1075.343)"/></svg>
        <p>You get emails every week with the deals we found for you.</p>
    </div>
    <div class="arrow">
        <div class="arrow__item"></div>
    </div>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="554 1077.333 34.866 34.866"><defs></defs><path class="a" d="M19.433,2A17.433,17.433,0,1,0,36.866,19.433a17.037,17.037,0,0,0-1.063-5.91l-2.789,2.789a13.508,13.508,0,0,1,.366,3.121A13.946,13.946,0,1,1,19.433,5.487a13.508,13.508,0,0,1,3.121.366L25.36,3.046A17.611,17.611,0,0,0,19.433,2m12.2,0L24.663,8.973v2.615l-4.445,4.445a2.413,2.413,0,0,0-.784-.087,3.487,3.487,0,1,0,3.487,3.487,2.413,2.413,0,0,0-.087-.784L27.278,14.2h2.615L36.866,7.23h-5.23V2m-12.2,6.973a10.46,10.46,0,1,0,10.46,10.46H26.406a6.973,6.973,0,1,1-6.973-6.973Z" transform="translate(552 1075.333)"/></svg>
        <p>Select the deal that best suits your time and budget.</p>
    </div>
    <div class="arrow">
        <div class="arrow__item"></div>
    </div>
    <div class="feature">
        <svg class="feature__icon" xmlns="http://www.w3.org/2000/svg" viewBox="968 1071.371 50.41 43.326"><defs></defs><path class="a" d="M3.481,41.925H50.709V46.9H3.481V41.925M52.126,18.658a3.72,3.72,0,0,0-4.574-2.635l-13.2,3.53L17.152,3.57,12.4,4.838,22.7,22.66,10.341,25.966l-4.9-3.828-3.6.969,4.524,7.855,1.914,3.306,3.977-1.044,13.2-3.555,10.813-2.883,13.2-3.5A3.821,3.821,0,0,0,52.126,18.658Z" transform="translate(966.16 1067.802)"/></svg>
        <p>You buy and fly cheaper than ever.</p>
    </div>
</section>
<section class="padding-section bg-color-lightergrey">
    <div class="deal deal--one">
        <div class="deal__body">
            <div class="deal__header">
                <h2>Latest flight deals:</h2>
                <!-- <p class="nomargin-bottom"><b>You will miss these if you are not a Premium member.</b></p>
                <p><a href="/signup.php">Become Premium</a> and don't miss any deal!</p> -->
            </div>
            <div class="deal__details">
                <h3>TO:</h3>
                <p>Barcelona (BCN)</p>
                <h3>FROM:</h3>
                <ul>
                    <li>San Francisco (SFO) - $400 </li>
                </ul>
                <h3>WHEN:</h3>
                <p>September 2019</p>
                <h3>STOPOVERS:</h3>
                <p>1</p>
                <h3>NORMAL PRICE</h3>
                <p>$900 - You save up to $500 with this deal</p>
            </div>
        </div>
    </div>
</section>
<section class="max-width padding-section">
<?php
include 'shared/prices.php';
?>
</section>
<section class="padding-section bg-color-lightergrey">
    <div class="deal deal--two">
        <div class="deal__body">
            <div class="deal__header">
                <h2>Latest ticket deals:</h2>
                <!-- <p class="nomargin-bottom"><b>You will miss these if you are not a Premium member.</b></p>
                <p><a href="/signup.php">Become Premium</a> and don't miss any deal!</p> -->
            </div>
            <div class="deal__details">
                <h3>TO:</h3>
                <p>San Francisco (SFO)</p>
                <h3>FROM:</h3>
                <ul>
                    <li>Barcelona (BCN) - $268 </li>
                    <li>Airline: Iberia</li>
                </ul>
                <h3>WHEN:</h3>
                <p>September 2019</p>
                <h3>STOPOVERS:</h3>
                <p>Non-stop</p>
                <h3>NORMAL PRICE</h3>
                <p>$100 / $1200 - You save up to $800 with this deal</p>
            </div>
        </div>
    </div>
</section>
<section class="padding-section grid justify-center">
<h2 class="grid--item-12 justify-spacebetween margin-bottom align-center">Keep up to date</h2>
    <a href="https://www.facebook.com/FlightMondo/" target="_blank" class="margin-sides ss ss--facebook">
        <?php
include 'resources/img/svgicons/facebook.svg';
?>
        <span>
        Facebook
        </span>
    </a>
    <a href="http://instagram.com/flightmondo" target="_blank" class="margin-sides ss ss--instagram">
        <?php
include 'resources/img/svgicons/instagram.svg';
?>
        <span>
        Instagram
        </span>
    </a>
</section>
<?php
include_once "shared/footer.php";
?>

</body>
</html>
