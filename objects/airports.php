<?php
class Product
{

    // database connection and table name
    private $conn;

    private $table_name = "airports";

    // object properties
    public $city;
    public $airportName;
    public $IATACode;
    public $passRole;

    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    // read products
    public function read()
    {

        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
}
