<?php
include_once "../../helper/session.php";
include_once "../../phpscript/conn.php";
include_once "../stripe-init.php";
include_once "../../shared/header-noindex.php";
?>
<body class="dashboard">

<?php
include_once "../../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
        <h1 class="block__title align-center">Recover your password.</h1>
        <div class="block__body">
<?php

if (isset($_POST["loginSubmit"])) {
    $email = $_POST["loginemail"];

    $sql = "SELECT user_email FROM users WHERE user_email='$email'";
    $results = mysqli_query($conn, $sql);
    if ($result = mysqli_query($conn, $sql)) {
        if (mysqli_num_rows($result) > 0) {

            while ($row = mysqli_fetch_array($result)) {
                $date = date('m/d/Y h:i:s a', time());
                $passtoken = password_hash($date, PASSWORD_DEFAULT);
                $sql = "UPDATE users SET user_resetpasstocken = '$passtoken', user_resetpasstime = NOW() WHERE user_email='$email'";
                $results = mysqli_query($conn, $sql);
                if ($result) {
                    $to = $email;
                    $subject = 'Your password reset request.';
                    $headers = "From: noreply@flightmondo.com" . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    $message = '<html><body>';
                    $message .= '<h4>Password Reset</h4>';
                    $message .= '<p>It is a password reset request from <a href="https://flightmondo.com">Flightmondo.com</a>.</p>';
                    $message .= '<p>Click on the following link to start reseting your password, if you cannot click it, copy it and paste it in your browser:</p>';
                    $message .= "<a href='https://flightmondo.com/user/area/changepass.php?email=" . $email . "&token=" . $passtoken . ">https://flightmondo.com/user/area/changepass.php?email=" . $email . "&token=" . $passtoken . "</a>";
                    $message .= "</body></html>";
                    mail($to, $subject, $message, $headers);

                    echo "<p class=\"alert\">We have sent a link to reset your password, click on it.</p><p class=\"font-small\">Remember to check SPAM folder</p>";
                }
            }

            // Free result set
            mysqli_free_result($result);
        } else {
            echo "<p class=\"alert\">Sorry we couldn't find that email.</p>";
        }
    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
} else {
    ?>

    <form method="post">
        <label for="loginemail">Email</label>
        <input type="email" class="margin-bottom" placeholder="email" name="loginemail" id="loginemail">
        </div>
        <div class="block__footer">
            <input type="submit" value="Reset Password" class="btn margin-bottom" name="loginSubmit">
            <p class="font-small nomargin-bottom"><a href="/user/area/login.php">Login</a> | <a href="/home/signup.php">Sign Up</a></p>
        </div>
    </form>
<?php
}
?>



	</section>
</div>
<?php
include_once "../../shared/footer.php";
?>

</body>
</html>