<?php
$ogTitle = "Flighmondo official tourism guides database";
$ogDescription = "A collection of official tourism guides worldwide.";
include_once "../../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey stickyfooter">
<?php
include_once "../../helper/session.php";
include_once "../../shared/topbar.php";
?>

<section class="max-width padding-section stickyfooter__content">
    <h1 class="grid--item-12 h2">Official Tourism Guides</h1>
    <p>We are collecting touristic guides from different cities so you can use in your travels (Hopefully, you get a good <a href="/"> flight deal </a>with us!). We will be updating with more soon.</p>
    <div class="sharethis-inline-share-buttons margin-bottom"></div>

    <table>
        <caption></caption>
        <thead>
            <tr>
                <th>Country</th>
                <th>City</th>
                <th>Available languages</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Australia</td>
                <td>Sydney</td>
                <td>
                    <a href="https://int.sydney.com/" target="_blank" title="English" class="flaglink au"><span class="hide-sr">English</span></a>
                    <a href="https://www.sydney.cn/" target="_blank" title="中文" class="flaglink cn"><span class="hide-sr">中文</span></a>
                    <a href="https://jp.sydney.com/" target="_blank" title="日本語" class="flaglink jp"><span class="hide-sr">日本語</span></a>
                    <a href="https://de.sydney.com/" target="_blank" title="Deutsch" class="flaglink de"><span class="hide-sr">Deutsch</span></a>
                </td>
            </tr>
            <tr>
                <td>China</td>
                <td>Beijing</td>
                <td>
                    <a href="http://english.visitbeijing.com.cn/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://visitbeijing.com.cn/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="http://german.visitbeijing.com.cn/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="http://spanish.visitbeijing.com.cn/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="http://russian.visitbeijing.com.cn/" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="http://french.visitbeijing.com.cn/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="http://japan.visitbeijing.com.cn/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>China</td>
                <td>Shanghai</td>
                <td>
                    <a href="http://www.meet-in-shanghai.net/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://chs.meet-in-shanghai.net/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="http://de.meet-in-shanghai.net/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>

                    <a href="http://fra.meet-in-shanghai.net/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="http://www.shanghaitrip.net/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>China</td>
                <td>Hong Kong</td>
                <td>
                    <a href="http://www.discoverhongkong.com/eng/index.jsp" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.discoverhongkong.com/cn-index.jsp" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="http://www.discoverhongkong.com/de/index.jsp" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="http://www.discoverhongkong.com/es/index.jsp" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="http://www.discoverhongkong.com/ru/index.jsp" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="http://www.discoverhongkong.com/fr/index.jsp" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="http://www.discoverhongkong.com/jp/index.jsp" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>France</td>
                <td>Paris</td>
                <td>
                    <a href="https://en.parisinfo.com/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="https://zh.parisinfo.com/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://de.parisinfo.com/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="https://es.parisinfo.com/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="https://ru.parisinfo.com/" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="https://www.parisinfo.com/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="https://ja.parisinfo.com/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>Japan</td>
                <td>Tokyo</td>
                <td>
                    <a href="https://www.gotokyo.org/en/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.gotokyo.org/cn/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="hhttp://www.gotokyo.org/de/index.html" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="http://www.gotokyo.org/es/index.html" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="http://www.gotokyo.org/fr/index.html" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="http://www.gotokyo.org/jp/index.html" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>Russia</td>
                <td>Moscow</td>
                <td>
                    <a href="https://www.lonelyplanet.com/russia/moscow" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                </td>
            </tr>
            <tr>
                <td>Singapore</td>
                <td>Singapore</td>
                <td>
                    <a href="http://www.visitsingapore.com/en/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.visitsingapore.com.cn/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="http://www.visitsingapore.com/de_de/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="http://www.visitsingapore.com/ja_jp/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>Spain</td>
                <td>Madrid</td>
                <td>
                    <a href="https://www.esmadrid.com/en" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="https://www.esmadrid.com/zh" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://www.esmadrid.com/de" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="https://www.esmadrid.com/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="https://www.esmadrid.com/ru" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="https://www.esmadrid.com/fr" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="https://www.esmadrid.com/ja" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>Spain</td>
                <td>Barcelona</td>
                <td>
                    <a href="http://www.barcelonaturisme.com/wv3/en/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.barcelonaturisme.com/wv3/es/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="http://www.barcelonaturisme.com/wv3/fr/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                </td>
            </tr>
            <tr>
                <td>United Arab Emirates</td>
                <td>Dubai</td>
                <td>
                    <a href="https://www.visitdubai.com/en/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="https://www.dubaitourism.cn/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://www.visitdubai.com/de/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="https://www.visitdubai.com/es/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="https://www.visitdubai.com/ru/" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="https://www.visitdubai.com/fr/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="https://www.visitdubai.com/ja/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>United Arab Emirates</td>
                <td>Abu Dhabi</td>
                <td>
                    <a href="https://visitabudhabi.ae/int-en/default.aspx" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="https://visitabudhabi.ae/int-ch/default.aspx" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://visitabudhabi.ae/int-de/default.aspx" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>

                    <a href="https://visitabudhabi.ae/int-ru/default.aspx" target="_blank" class="flaglink ru" title="Pусский"><span class="hide-sr">Pусский</span></a>
                    <a href="https://visitabudhabi.ae/int-fr/default.aspx" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="https://visitabudhabi.ae/int-jp/default.aspx" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>United Kingdom</td>
                <td>London</td>
                <td>
                    <a href="https://www.visitlondon.com/" target="_blank" class="flaglink gb" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://london.cn/2016/07/21/visit-london/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://www.visitlondon.com/de?ref=header" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="https://www.visitlondon.com/es?ref=header" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="https://www.visitlondon.com/fr?ref=header" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                </td>
            </tr>
            <tr>
                <td>United States</td>
                <td>New York</td>
                <td>
                    <a href="https://www.nycgo.com/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.gousa.cn/nyc" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                </td>
            </tr>
            <tr>
                <td>United States</td>
                <td>Los Angeles</td>
                <td>
                    <a href="https://www.discoverlosangeles.com/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.hellola.cn/" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="https://de.discoverlosangeles.com/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="https://es.discoverlosangeles.com/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="https://fr.discoverlosangeles.com/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="https://jp.discoverlosangeles.com/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
            <tr>
                <td>United States</td>
                <td>San Francisco</td>
                <td>
                    <a href="http://www.sftravel.com/" target="_blank" class="flaglink us" title="English"><span class="hide-sr">English</span></a>
                    <a href="http://www.gousa.cn/sanfrancisco" target="_blank" class="flaglink cn" title="中文"><span class="hide-sr">中文</span></a>
                    <a href="http://de.sftravel.com/" target="_blank" class="flaglink de" title="Deutsch"><span class="hide-sr">Deutsch</span></a>
                    <a href="http://es.sftravel.com/" target="_blank" class="flaglink es" title="Español"><span class="hide-sr">Español</span></a>
                    <a href="http://fr.sftravel.com/" target="_blank" class="flaglink fr" title="Français"><span class="hide-sr">Français</span></a>
                    <a href="http://jp.sftravel.com/" target="_blank" class="flaglink jp" title="中文"><span class="hide-sr">日本語</span></a>
                </td>
            </tr>
        </tbody>
    </table>
</section>

<?php
include_once "../../shared/footer.php";
?>

</body>
</html>
