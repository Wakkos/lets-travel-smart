<?php
include_once "../helper/session.php";
include_once "../shared/header-noredirect.php";
?>
<body itemscope itemtype="https://schema.org/Service" class="stickyfooter">
<?php
include_once "../shared/topbar.php";
?>
<header>

</header>

<section class="max-width padding-section stickyfooter__content">
<?php
include '../shared/prices.php';
?>
</section>

<?php
include_once "../shared/footer.php";
?>

</body>
</html>
