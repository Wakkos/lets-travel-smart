
<!doctype html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js dashboard-bg" lang="en"><!--<![endif]-->

<head>
<?php
  $analyticsPath = $_SERVER['DOCUMENT_ROOT'];
  $analyticsPath .= "/shared/analytics.php";
  include_once($analyticsPath);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Flightmondo: flight deals in your email</title>


<!-- http://bit.ly/18VB51x -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- CSS Para todos los navegadores -->
<link rel="stylesheet" href="/resources/css/style.css">

<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="css/lt-ie9.css">
<![endif]-->


<!-- Iconos -->



<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|Roboto:400,700" rel="stylesheet">
</head>

<body class="dashboard">