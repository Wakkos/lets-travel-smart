<?php
include_once "../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey">
<?php
include_once "../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
        <h1 class="block__title align-center">Get cheap airtickets right in your email</h1>
        <div class="block__body">
            <p>We will send offers and deals on airtickets directly to your email. Sign up now to save hundreds of dollars per ticket!</p>
            <!-- Begin MailChimp Signup Form -->

            <div id="mc_embed_signup">
            <form action="https://flightmondo.us18.list-manage.com/subscribe/post?u=13071d1ff0da39c0493798cf8&amp;id=d95b4ce27d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">

            <div class="margin-bottom">
                <label for="mce-EMAIL">Email Address </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_13071d1ff0da39c0493798cf8_d95b4ce27d" tabindex="-1" value=""></div>
                <div class="align-right">
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">
                </div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
        </div>




    </section>
</div>
<?php
include_once "../shared/footer.php";
?>

</body>
</html>
