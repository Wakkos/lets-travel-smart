<?php
include_once "../helper/session.php";
include_once "../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey">
<?php
include_once "../shared/topbar.php";
?>

<section class="max-width padding-section stickyfooter">
    <h2 class="align-center margin-bottom-xl grid--item-12 ">FAQ's</h2>
    <div class="collapse">
        <div class="collapse__item">
            <input type="checkbox" id="collapse1" class="collapse__radio">
            <label for="collapse1" class="collpase__title">
            How does FlightMondo work?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
                FlightMondo team searches for international flight deals using as a premise 2 main factors, prices at least 50% off and airports matching your criteria. The information is sent directly to your email. You will no longer need to spend time looking for deals, we do it for you!
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse2" class="collapse__radio">
            <label for="collapse2" class="collpase__title">
                How do I sign up?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            You can click here to <a href="singup.php">SIGN UP!</a> There you can choose your preferred plan (Annual or 3 month subscription) and set up your password. We will send you an email to confirm your email address, and once confirmed, you’ll be ready to go. Once signed up, you will be able to select your departure airports by country or search by airport name, city or airport code; and then start receiving airline tickets deals.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse3" class="collapse__radio">
            <label for="collapse3" class="collpase__title">
                How many emails should I expect to receive?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            That is proportional to the number of airports that you have selected. Premium members should expect about 5 times more deals than free accounts.

            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse4" class="collapse__radio">
            <label for="collapse4" class="collpase__title">
            Are You Going To Fill My Inbox With Worthless Deals?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Absolutely Not. FlightMondo’s aim is to provide a customized service to help you reach your travel dreams and needs, based on your selected airports, and at prices which are about 50% OFF off of retail.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse5" class="collapse__radio">
            <label for="collapse5" class="collpase__title">
            What is unique about FlightMondo?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            We are a team of experienced travelers who want to build a customized global HUB for flight deals. We understand that each customer has different needs and situations, and have developed different techniques to find, distribute and communicate the best flight deals available and pass them on to you!
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse6" class="collapse__radio">
            <label for="collapse6" class="collpase__title">
            Will There Be Airline Tickets Deals For Airports Near Me?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes. You will only receive emails that match your selected airports.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse7" class="collapse__radio">
            <label for="collapse7" class="collpase__title">
            Which countries do the deals originate from?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Our service has no limitation when it comes to specific airports. If a great deal exists and matches your criteria, we will send it to you!
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse8" class="collapse__radio">
            <label for="collapse8" class="collpase__title">
            Can You Help Me Find An Airticket Deal To A Particular Destination?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes, BUT this is subject to the availability of our advisors. This is a limited service not included in your premium subscription. If you are interested on this kind of service, please contact us.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse9" class="collapse__radio">
            <label for="collapse9" class="collpase__title">
            Are these last-minute deals?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Generally no. But there is a possibility that you will recieve some last minute offers. Most of the emails you will receive are for travel dates from 3 to 10 months in the future. Flexibility is the key to getting the best deal.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse10" class="collapse__radio">
            <label for="collapse10" class="collpase__title">
            Is my credit card info protected?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes. All payments are processed through Stripe, a US-based company used by millions of companies in over 120 countries. All card numbers are encrypted on disk with AES-256. For official information please read: <a href="https://stripe.com/docs/security/stripe" target="_blank">https://stripe.com/docs/security/stripe</a>
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse11" class="collapse__radio">
            <label for="collapse11" class="collpase__title">
            I signed up. Why am I not receiving emails?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            It’s possible that our emails are going directly into your spam folder. In this link you will find instructions on how to keep FlightMondo emails out of the spam folder: <a href="https://mailchimp.com/help/about-safe-sender-lists/" target="_blank">https://mailchimp.com/help/about-safe-sender-lists/</a> .
            If you use Gmail, please check the tabs “Promotions” or "Updates". If none of these solutions have worked, please contact us at contact@flightmondo.com, and we will help you resolve the problem.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse12" class="collapse__radio">
            <label for="collapse12" class="collpase__title">
            Will I only receive deals for Economy tickets?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Our goal is to find deals that are at least 50% OFF of retail prices, including Business and/or First Class. Thus, occasionally you will receive deals for the most expensive classes.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse13" class="collapse__radio">
            <label for="collapse13" class="collpase__title">
            Do You Send Out Deals For Domestic Flights?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Our service is mainly intended for international travelers. But from time to time, we will send out domestic deals for USA and China. For example: East Coast to West Coast for USA, USA Mainland to Puerto Rico, Hawaii, Alaska or Guam. China Mainland to Hong Kong.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse14" class="collapse__radio">
            <label for="collapse14" class="collpase__title">
            Does the Premium paid subscription renew automatically?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes, your subscription will be renewed based on the plan you signed up for. 3-Month renews every 3 months,  and annual renews every year. You can cancel at anytime, no questions asked.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse15" class="collapse__radio">
            <label for="collapse15" class="collpase__title">
            What is your refund policy?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            We offer a guaranteed no-questions asked 30-day 100% money-back. You can read our detailed refund policy <a href="/home/legal/flight-deals-refund.php" target="_blank">here</a>
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse16" class="collapse__radio">
            <label for="collapse16" class="collpase__title">
            May I book multiple tickets with your deals?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes. The emails you will receive contain a link which allows you to select the quantity of tickets, but is subject to the availability of the flight.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse17" class="collapse__radio">
            <label for="collapse17" class="collpase__title">
            May I get a receipt for my payment?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            Yes. Please send us an email at contact@FlightMondo.com
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse18" class="collapse__radio">
            <label for="collapse18" class="collpase__title">
            How may I cancel my premium subscription?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            If you would like to cancel, Login to your account and click on "Change", next to subscription on the left side of your Dashboard. Thereafter you will receive an email confirming that your subscription will not be renewed automatically. Please note that you will still continue to receive ticket deal emails from our service until the actual end date of your paid plan.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse19" class="collapse__radio">
            <label for="collapse19" class="collpase__title">
            May I See The Deals On My Dashboard within the website?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            No. You will receive all deals via email, to the email address you provided when signing up.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse20" class="collapse__radio">
            <label for="collapse20" class="collpase__title">
            How do I change which airport(s) I want to receive alerts from?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            You have to Log In to your account, on the Dashboard there is a section "Your Departure Airports", you can click "Add by Country" or "Search Airports". Then you will be able to select your departure airports by country or search by airport name, city or airport code; and then start receiving airline tickets deals.
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse21" class="collapse__radio">
            <label for="collapse21" class="collpase__title">
            How can I contact you?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            You can fill our <a href="/home/legal/flight-deals-contact.php">contact form</a>, or you can send an email to contact@FlightMondo.com
            </p>
        </div>
        <div class="collapse__item">
            <input type="checkbox" id="collapse22" class="collapse__radio">
            <label for="collapse22" class="collpase__title">
            How may I reserve and/or pay for the flight?
            </label>
            <svg class="iconsvg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <g>
                    <polygon id="path-1_1_" points="2.8,4.6 0,7.4 12,19.4 24,7.4 21.2,4.6 12,13.8 	"/>
                </g>
            </svg>
            <p class="collapse__text">
            On the email you will receive, there are links to search samples where you can pay directly to the airline, or through online travel agencies (OTA), e.g: Google Flights, Momondo, etc.
            </p>
        </div>
    </div>




</section>

<?php
include_once "../shared/footer.php";
?>

</body>
</html>
