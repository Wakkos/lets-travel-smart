<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once "../../shared/header-noredirect.php";
?>
<body class="dashboard bg-color-lightergrey">
<?php
include_once "../../shared/topbar.php";
?>

<section class="max-width padding-section stickyfooter">
    <div class="block block--payment centered-body">
        <h2 class="block__title align-center">Get in touch</h2>
        <div class="block__body">
            <p>If you have any questions, complaints, or comments fill in the form below.</p>
<?php
if (isset($_POST['submit'])) {
    ini_set('display_errors', 1);

    error_reporting(E_ALL);

    $from = "FlightMondo";

    $to = "wakkos@gmail.com, alexgzlz@gmail.com";

    $subject = "Formulario de Contacto Flightmondo";

    $message = $_POST['userMessage'];

    $headers = "From:" . $from;

    mail($to, $subject, $message, $headers);

    echo "<div class=\"alert--success\">Your message has been sent. We'll get in touch with you shortly!</div>";
}
?>
            <form action="" method="post">
                <label for="">Email</label>
                <input type="email" class="margin-bottom" name="userEmail" required>
                <label for="">Message</label>
                <textarea name="userMessage" cols="30" rows="10" class="margin-bottom" required></textarea>
                <div class="align-right">
                    <input type="submit" class="btn" name="submit" value="Submit">
                </div>
            </form>
        </div>
    </div>




</section>

<?php
include_once "../../shared/footer.php";
?>

</body>
</html>
