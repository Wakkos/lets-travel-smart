<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once "../stripe-init.php";
// if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != true) {
//     header('location: /index.php');
// }
include_once '../header.php';
?>

<?php
include_once "../../shared/topbar.php";
?>
<div class="stickyfooter ">
	<div class="breadcrumb block--medium">
        <a href="../dashboard.php" class="breadcrumb__item">Dashboard</a>
        <a href="../settings.php" class="breadcrumb__item">Settings</a>
        <span class="breadcrumb__item">Change Credit Card</span>
    </div>
    <section class="block block--payment centered-body">
        <h2 class="block__title">Change credit card</h2>
        <?php
if (isset($_POST['stripeToken'])) {
    $email = $_SESSION['email'];
    $sql = "SELECT user_stripeId FROM users WHERE user_email='$email'";
    $results = mysqli_query($conn, $sql);
    if ($result = mysqli_query($conn, $sql)) {
        while ($row = mysqli_fetch_array($result)) {
            $stripe_id = $row['user_stripeId'];
        }

    } else {
        echo "ERROR: a weird error happened. Try again please.";
    }
    try {
        $cu = \Stripe\Customer::retrieve($stripe_id); // stored in your application
        $cu->source = $_POST['stripeToken']; // obtained with Checkout
        $cu->save();

        $success = "<div class=\"alert--success\">Your card details have been updated!</div>";
        echo $success;
    } catch (\Stripe\Error\Card $e) {

        // Use the variable $error to save any errors
        // To be displayed to the customer later in the page
        $body = $e->getJsonBody();
        $err = $body['error'];
        $error = $err['message'];
        echo $error;
    }
    // Add additional error handling here as needed

}
?>
        <form action="" method="post" class="stripe" id="js-emailform">
            <div class="block__body">
                <label>Credit Card details</label>
				<div id="card-element" class="margin-bottom-xl">
				<!-- A Stripe Element will be inserted here. -->
				</div>
				<!-- Used to display Element errors. -->
				<div id="card-errors" role="alert"></div>

            </div>
            <div class="block__footer">
                <a href="dashboard.php" class="btn--secondary">Go back</a> <input type="submit" value="Update Credit Card" class="btn">
            </div>
        </form>
    </section>
</div>

<?php
include_once "../../shared/footer.php";
?>
<script src="https://js.stripe.com/v3/"></script>
<script>
var stripe = Stripe('pk_test_I2S1nIeZ3srPaU0qC4m7PNvn');
var elements = stripe.elements();


// Create an instance of the card Element.
var card = elements.create('card', {
	iconStyle: 'solid',
    style: {
      base: {
        iconColor: 'black',
        color: '#586368',
        fontWeight: 500,
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',

        ':-webkit-autofill': {
          color: '#fce883',
        },
        '::placeholder': {
          color: '#bfbfbf',
        },
      },
      invalid: {
        iconColor: 'rgb(218,79,73)',
        color: 'rgb(218,79,73)',
      },
    },
});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');


card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
    displayError.className += " alert--error";
  } else {
    displayError.textContent = '';
    displayError.classList.remove("alert--error");
  }
});


// Create a token or display an error when the form is submitted.
var form = document.getElementById('js-emailform');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('js-emailform');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
</body>
</html>
