<?php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] = true) {
    header('location: /user/dashboard.php');
}
?>
<!doctype html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
<link rel="alternate" hreflang="en-us" href="https://flightmondo.com" />

<!-- Cookie Consent by https://PrivacyPolicies.com -->
<script type="text/javascript" src="//PrivacyPolicies.com/cookie-consent/releases/2.0.0/cookie-consent.js"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
    cookieconsent.run({"notice_banner_type":"headline","consent_type":"implied","palette":"dark"});
});
</script>

<?php
  $analyticsPath = $_SERVER['DOCUMENT_ROOT'];
  $analyticsPath .= "/shared/analytics.php";
  include_once($analyticsPath);
?>

<noscript>Cookie Consent by <a href="https://privacypolicies.com/">Privacy Policies</a></noscript>
<!-- End Cookie Consent -->
<meta charset="utf-8">

<title>Flightmondo: flight deals in your email.</title>
<meta name="description" content="We will make you save 50% or more in your next airtickets.">

<!-- http://bit.ly/18VB51x -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">


<?php
  include_once "partials/facebook-meta.php";
?>
<!-- CSS Para todos los navegadores -->
<link rel="stylesheet" href="/resources/css/style.css">




<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- OneTrust Cookies Consent Notice start -->

<script type="text/javascript">

function OptanonWrapper() { }

</script>

<!-- OneTrust Cookies Consent Notice end -->
</head>
