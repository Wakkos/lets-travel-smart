<?php
include_once "../shared/header.php";
include_once "../user/stripe-init.php";
?>

<body class="dashboard">
<?php
include_once "../shared/topbar-nologin.php";
?>
<div class="stickyfooter">
    <section class="block block--payment block--centered margin-bottom">
		<h1 class="block__title align-center">Welcome.</h1>
		<div class="block__body">
<?php

SignupUser();

function SignupUser()
{
    include "../phpscript/conn.php";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $error = false;
        $email = $_POST['finalEmail'];
        $user_password = $_POST['password'];

        $stripeToken = $_POST['stripeToken'];
        if (isset($_POST['subscription'])) {
            $subscription = $_POST['subscription'];
        } else {
            $subscription = 1;
        }

        $sql = "SELECT * FROM users WHERE user_email='$email'";
        $results = mysqli_query($conn, $sql);
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) != 0) {
                echo "<p class='alert--error margin-bottom'>That email already exists</p> <div class=\"align-center\"><a href=\"signup.php\" class=\"btn\">Go Back</a>";
            } else {
                try {
                    if (ValidateUserSignup($stripeToken, $email, $subscription)) {
                        $coupon = "";
                        if ($_POST['coupon_id'] != "") {
                            $answer = false;
                            $coupon_id = $_POST['coupon_id'];

                            // needs if coupon_id is not blank
                            try {
                                $coupon = \Stripe\Coupon::retrieve($coupon_id);
                            } catch (Stripe_InvalidRequestError $e) {
                                // $answer is already set to false

                            }

                            if ($coupon->valid) {
                                $answer = true;
                            }
                            if ($subscription == 1) {
                                $customer = CreateStripeClient( $stripeToken, $email, 'planannual', $coupon);
                            } else if ($subscription == 2) {
                                $customer = CreateStripeClient( $stripeToken, $email, 'plantrimestral', $coupon);
                            }
                        } else {

                            if ($subscription == 1) {
                                $customer = CreateStripeClient( $stripeToken, $email, 'planannual');
                            } else if ($subscription == 2) {
                                $customer = CreateStripeClient( $stripeToken, $email, 'plantrimestral');
                            }
                        }
                        include "./../helper/mailchimpfunctions.php";
                        Signup($email, $subscription);



                        //Create customer in DB
                        AddUserToDatabase($email, $user_password, $customer,$subscription);

                    } else {
                        throw new Exception("The Stripe Token or customer was not generated correctly");
                    }
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                if (!$error) {

                    echo (' <p>You can now login and start getting offers and flight deals to your dream destinations. Please, <a href="/user/area/login.php">Log In</a> and choose your departure airports.</p>
                            ');

                } else {

                    echo "<div class=\"alert--error margin-bottom\">" . $error . "</div><div class=\"align-center\"><a href=\"signup.php\" class=\"btn\">Go Back</a>";

                }
            }
        }
    }
}

function AddUserToDatabase($email, $user_password, $customer,$subscription)
{
    include "../phpscript/conn.php";

 // fix stripe id
    //$stripe_id = $customer->id;

    foreach($customer as $result) {
        $stripe_id = $result;
    }

    if ("" == trim($email) && "" == trim($password)) {
        $result .= "<div class='alert--error font-small'>Email or password can't be blank</div>";
    } else {
        $hased_pass = password_hash($user_password, PASSWORD_DEFAULT);

        $sql = "INSERT INTO users (user_email, user_password, user_joindate, user_subscription, user_stripeId) VALUES ('$email', '$hased_pass', NOW(), '$subscription', '$stripe_id')";

        if ($conn->query($sql) === true) {
            $result = "Registration Successful!" . $subscription . "";
            $sql = "SELECT * FROM users WHERE user_email='$email'";
            $results = mysqli_query($conn, $sql);
            if ($result = mysqli_query($conn, $sql)) {
                if (mysqli_num_rows($result) > 0) {

                    while ($row = mysqli_fetch_array($result)) {
                        $id = $row['user_id'];
                        $hased_pass = $row['user_password'];
                        $email = $row['user_email'];
                        if (password_verify($password, $hased_pass)) {
                            //User created correctly
                            // User Charged
                            // USer added to Mailchimp

                        } else {
                            $loginResult .= "<div class='alert--error font-small'>Email or password can't be blank</div>";
                        }
                    }

                    // Free result set
                    mysqli_free_result($result);
                } else {
                    $loginResult .= '<div class="autocomplete__item alert--warning"> No records matching your query were found.</div>';
                }
            } else {
                $loginResult .= "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
            }

        } else {
            echo "<script>console.log('query failed:'" . $conn->error . ")</script>";
            $result = "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

function SubscribeToMailChimp($email, $subscription)
{
    // Add to Mailchimp list
    include "./../helper/mailchimpvars.php";
    // member information

    $json = json_encode([
        'email_address' => $email,
        'status' => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
        'merge_fields' => [
            'FNAME' => $email,
            'LNAME' => $email,
            'PREMIUM' => $subscription,
        ],
    ]);
    // send a HTTP POST request with curl
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);
    $mchttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    // store the status message based on response code
    if ($mchttpCode == 200) {
        $_SESSION['msg'] = '<p style="color: #34A853">You have successfully subscribed.</p>';
    } else {
        switch ($mchttpCode) {
            case 214:
                $_SESSION['msg'] = 'You are already subscribed.';
                break;
            default:
                $_SESSION['msg'] = 'Error adding to Mailchimp with email, not a 214.';
                break;
        }
        $_SESSION['msg'] = '<p style="color: #EA4335">' . $_SESSION['msg'] . '</p>';
    }
}

function ValidateUserSignup($stripeToken, $email, $subscription)
{
    if (!IsNullOrEmptyString($stripeToken) && !IsNullOrEmptyString($email) && !IsNullOrEmptyString($subscription)) {
        return true;
    }

    return false;
}

function IsNullOrEmptyString($string)
{
    if (isset($string) === true && trim($string) === '') {
        return true;
    } else {
        return false;
    }
}

function CreateStripeClient($stripeToken, $email, $plan, $coupon = null)
{

    $res = strpos($stripeToken, 'dummy');
    if (strpos($stripeToken, 'dummy') !== 0) {

        $customer = array('source' => $stripeToken,
            'email' => $email,
            'plan' => $plan);

        if ($coupon != null) {
            $customer["coupon"] = $coupon;
        }

        return \Stripe\Customer::create($customer);
    }else{

        $newid = substr($stripeToken,-5);

        return $customare = array("id"=>$newid);
    }


}

?>
	</div>
	</section>
</div>
<?php
include_once "../shared/footer.php";
?>