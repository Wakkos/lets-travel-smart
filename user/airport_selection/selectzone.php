<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once '../header.php';

?>



<div class="breadcrumb">
    <a href="selectcountry.php" class="breadcrumb__item">Select Country</a>
     <span class="breadcrumb__item">Select Zone</span>
</div>

<div class="stickyfooter">
    <section class="max-width block margin-bottom padding" id="js-hasAlert">
        <h2 class="page-header margin-bottom">Choose your airports.</h2>
        <p>We will send you offers from the airports you choose.</p>
        <form action=""  method="post">
            <?php
$user_id = $_SESSION['id'];
$airportIdInUser[] = array();
if (isset($_GET['country'])) {

    $country = $_GET['country'];
    $data = mysqli_query($conn, "SELECT count(alpha2_zones) AS zonenumber from zones WHERE alpha2_zones = '$country'");
    // A COUNT query will always return 1 row
    // (unless it fails, in which case we die above)
    // Use fetch_assoc for a nice associative array - much easier to use
    $row = mysqli_fetch_assoc($data);

    // Get the number of uses from the array
    // 'num' is what we aliased the column as above
    $zoneUsers = $row['zonenumber'];

    if ($zoneUsers > 1) {
        ?>
                        <div class="grid bg-color-grey">
                        <?php
$zones = "select * from zones where alpha2_zones = '$country'";
        if ($result = mysqli_query($conn, $zones)) {
            while ($row = mysqli_fetch_array($result)) {
                echo "<div class=\"zonemap\">";
                echo "<img src=\"/img/maps/" . $row['id_zones'] . ".svg\">";
                echo "<p>" . $row['name_zones'] . "</p>";
                echo "</div>";
            }
        }
        ?>
                        </div>
                        <?php
} else {
        $userAirports = " SELECT airports_usersairports from users_airports where users_usersairports = '$user_id'";
        if ($result = mysqli_query($conn, $userAirports)) {
            while ($row = mysqli_fetch_array($result)) {
                $airportIdInUser[] = $row['airports_usersairports'];
            }
        }
        $sql = "SELECT * FROM airports
                        left join cities on cities.name_cities = airports.temp_city
                        left join zones on cities.zone_cities = zones.name_zones
                        where alpha2_zones ='$country'
                        ";
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    if (in_array($row['id_airport'], $airportIdInUser)) {
                        $isChecked = "checked";
                    } else {
                        $isChecked = null;
                    }
                    echo "<div class='grid--item-6 checkbox margin-bottom'><input type='checkbox' " . $isChecked . " class='js-checkbox' name='iata[]' value='" . $row['id_airport'] . "' id='" . $row['iata_airport'] . "'><label for='" . $row['iata_airport'] . "' class='checkbox__label'>" . $row['name_cities'] . " - " . $row['name_airport'] . "</label></div>";
                }

                // Free result set
                mysqli_free_result($result);
            } else {
                echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
    }

} else {
    echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
}
// There has to be a country selected from previous page or
// already added to session if you are reloading.
/*
if(isset($_POST['country'])) {
$email = $_SESSION['email'];
$country = $_POST['country'];

// If you click on submit but select no airport, we warn you
// We get here because wqe already have Country added to session
?>
<form action="" method="post" onsubmit="return checkbox()">
<div class="grid margin-bottom">
<?php

//   $sql = "SELECT *, GROUP_CONCAT(DISTINCT '<div class=\"radioflag\ ',alpha_2,'\">',country,'</div>' SEPARATOR '')
$sql = "SELECT *
FROM airports
WHERE alpha_2 = '$country'";
if($result = mysqli_query($conn, $sql)){
if(mysqli_num_rows($result) > 0){
while($row = mysqli_fetch_array($result)){
echo "<div class='grid--item-6 checkbox margin-bottom'><input type='checkbox' class='js-checkbox' name='iata[]' value='" . $row['airports_id'] . "' id='" . $row['IATACode'] . "'><label for='" . $row['IATACode'] . "' class='checkbox__label'>" . $row['city'] . " - " . $row['airportName'] . "</label></div>";
}

// Free result set
mysqli_free_result($result);
} else{
echo '<div class="alert--warning"> No records matching your query were found.</div>';
}
} else{
echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
}
}
else {
echo "<div class='alert--error'>You haven't select any airport, or there was an error, please, <a href='selectcountry.php'>try again.</a></div>";
}
 */
?>
            <div class="align-right">
                <input type="submit" class="btn" value="add airports" id="js-addAirports">
            </div>
        </form>

    </section>

</div>



<?php
include_once "../..shared/footer.php";
?>


<script type="text/javascript">

// //Autocomplete function
// $(document).ready(function() {
//     //Al escribr dentro del input con id="service"
//     $('#js-userlocations').on('input',function(){
//         //Obtenemos el value del input
//         var service = $(this).val();
//         var dataString = 'service='+service;

//         //Le pasamos el valor del input al ajax
//         $.ajax({
//             type: "POST",
//             url: "/autocomplete.php",
//             data: dataString,
//             success: function(data) {
//                 //Escribimos las sugerencias que nos manda la consulta
//                 $('#js-suggestions').fadeIn(1000).html(data);
//                 $('#test').html(data);
//                 //Al hacer click en alguNa de las sugerencias
//                 $('#js-suggestions').on('click', '.suggest-element', function(){
//                     //Obtenemos la id unica de la sugerencia pulsada
//                     var id = $(this).attr('id');
//                     //Editamos el valor del input con data de la sugerencia pulsada
//                     $('#js-userlocations').val(id);
//                     //Hacemos desaparecer el resto de sugerencias
//                     $('#js-suggestions').fadeOut(1000);
//                     //eLIMINAMOS EL VALUE DEL INPUT
//                 });
//             }
//         });
//     });
// });

// //Add location function

// window.onload = function() {
//     document.getElementById("js-addLocation").onclick = function addLocation() {
//         var badgeExists = document.getElementById("js-airportBadge");

//                 if (badgeExists != null) {
//                     alert("You cannot add more");
//                     return;
//                 }
//         var locationValue = document.getElementById('js-userlocations').value;
//         var stringtoSplit = locationValue;
//         var splitstring = stringtoSplit.split(' - ');
//         fetch('/airport-verification.php', {
//             headers: {
//                 'Content-Type': 'application/x-www-form-urlencoded'
//             },
//             method: "POST",
//             body:  "locationValue=" + splitstring[0]
//         })
//         .then(function(response) {
//         return response.text();
//         })
//         .then(function(data) {

//             if (data === "invalid") {
//                 console.log("No Existe, es " + data);
//             }
//             else {
//                 console.log("Existe, es " + data);
//                 var tag = document.createElement("div");
//                 var icon = document.getElementById('blockOfStuff').innerHTML;

//                 tag.setAttribute("class", "airportBadge");
//                 tag.setAttribute("id", "js-airportBadge");
//                 tag.setAttribute("onclick", "DeleteBadge(this);");
//                 document.getElementById("js-locations").appendChild(tag);
//                 tag.innerHTML = splitstring[0] + '<span class="airportBadge__city">' + splitstring[1] + '</span>' + icon;


//                 // Create hidden input
//                 var input = document.createElement("input");
//                 input.setAttribute("type", "hidden");
//                 input.setAttribute("name", "airport");
//                 input.setAttribute("value", locationValue);
//                 document.getElementById("js-locations").appendChild(input);
//                 document.getElementById("js-addLocation").className = "btn--disabled";
//                 document.getElementById("js-addLocation").title = "You can add only one with our free account";
//             }
//         }).catch(function(error) {
//             console.log('Error: ' + error);
//         });
//         // Clear the input
//         document.getElementById("js-userlocations").value = "";
//     }

// }

    function checkbox(){

        if (document.querySelector('.js-checkbox:checked')) {
            console.log('foo')
        } else {
                var tag = document.createElement("div");
                tag.setAttribute("class", "airportBadge");
                tag.setAttribute("id", "js-airportBadge");
                tag.setAttribute("onclick", "DeleteBadge(this);");
                document.getElementById("js-hasAlert").appendChild(tag);
                tag.innerHTML = '<span class="alert--error">you have to select at least one airport.</span>';
            return false;
        }
    }

</script>
</body>
</html>