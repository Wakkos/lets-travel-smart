<?php
include_once "../helper/session.php";
include_once '../phpscript/conn.php';
include_once "stripe-init.php";
include_once 'header.php';
$userID = $_SESSION['id'];
?>

<?php
include_once "../shared/topbar.php";
?>
<div class="stickyfooter">

    <div class="breadcrumb block--medium">
        <a href="dashboard.php" class="breadcrumb__item">Dashboard</a>
        <span class="breadcrumb__item">Settings</span>
    </div>



    <main class="block block--payment centered-body">

        <h1 class="block__title">
            Settings
        </h1>
        <div class="block__body">

            <?php
$email = $_SESSION['email'];
$sql = "SELECT user_stripeId, user_unsubscribed, user_joindate FROM users WHERE user_email='$email'";
$results = mysqli_query($conn, $sql);
if ($result = mysqli_query($conn, $sql)) {
    while ($row = mysqli_fetch_array($result)) {
        $stripe_id = $row['user_stripeId'];
        $userMarkedToUnsubscribe = $row['user_unsubscribed'];
        $userJoinDate =  date('Y-m-d', strtotime($row['user_joindate']));

    }

} else {
    echo "ERROR: a weird error happened. Try again please.";
}
//Removed for beta version

// $user = \Stripe\Customer::retrieve($stripe_id);

// $user_current_period_end = $user->subscriptions->data[0]->current_period_end;
// $user_subscription_id = $user->subscriptions->data[0]->id;
// $user_cc_last4 = $user->sources->data[0]->last4;
// $user_cc_brand = $user->sources->data[0]->brand;
// $user_plan_id = $user->subscriptions->data[0]->items->data[0]->plan->id;
// $subscription = \Stripe\Subscription::retrieve($user_subscription_id);
?>
            <dl class="dl-horizontal dl-horizontal--pushright">
                <dt>Email:</dt>
                <dd><?php echo $_SESSION['email']; ?></dd>

                <dt>Subscription:</dt>
                <dd>Premium

                </dd>
                <dt>Join date:</dt>
                <dd><?php echo date("d/m/y", strtotime($userJoinDate));?> </dd>
                <dt>Subscription expire date:</dt>
                <dd><?php echo $userUnsubscriptionDate = date('d/m/y', strtotime("+3 months", strtotime($userJoinDate))); ?> </dd>
            </dl>
        </div>
        <div class="block__footer block__footer--sides">
        <?php if(!$userMarkedToUnsubscribe): ?>
            <button class="btn--warning" id="modalVerify">Unsubscribe</button>
        <?php else: ?>
            <button class="btn">Suscribe again</button>
        <?php
            endif;
        ?>
            <a href="dashboard.php" class="btn--secondary">Go back</a>
        </div>

    </main>
</div>
<div class="modal modal--info" id="ModalAlertInfo">
	<div class="modal__container">
		<div class="modal__title">
			Are you sure you want to unsuscribe?

        </div>
        <div class="modal__body">
            You will be able to use Flightmondo until <b><?php echo $userUnsubscriptionDate = date('d/m/y', strtotime("+3 months", strtotime($userJoinDate))); ?></b>
        </div>
		<div class="modal__actions">
            <a href="/user/area/unsubscribe.php" class="btn">YES</a>
            <button class="btn--secondary" id="modalVerifyClose">Cancel</button>
		</div>
	</div>
</div>
<?php
include_once "../shared/footer.php";
?>
<script type="text/javascript">

    function openModal(){
        // Code examples from above



        if ( document.getElementById("ModalAlertInfo").className.match(/(?:^|\s)modal--isopen(?!\S)/) ) {
            document.getElementById("ModalAlertInfo").className = document.getElementById("ModalAlertInfo").className.replace( /(?:^|\s)modal--isopen(?!\S)/g , '' )
        }
        else {
            document.getElementById("ModalAlertInfo").className += " modal--isopen";
        }
    }
    window.onload = function(){
        document.getElementById("modalVerify").addEventListener( 'click', openModal);
        document.getElementById("modalVerifyClose").addEventListener( 'click', openModal);
    }

</script>
</body>
</html>
