<?php
	if (isset($ogUrl) == false) {
		$ogUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}
	if (isset($ogType) == false) {
		$ogType = "article";
	}
	if (isset($ogTitle) == false) {
		$ogTitle = "Flightmondo: Flight deals for you";
	}
	if (isset($ogDescription) == false) {
		$ogDescription = "We will save you hundreds of dollars on your next flight(s).";
	}
	if (isset($ogImage) == false) {
		$ogImage = "https://flightmondo.com/resources/img/favico.png";
	}

?>
<meta property="og:url"                content="<?php echo $ogUrl ?>" />
<meta property="og:type"               content="<?php echo $ogType ?>" />
<meta property="og:title"              content="<?php echo $ogTitle ?>" />
<meta property="og:description"        content="<?php echo $ogDescription?>" />
<meta property="og:image"              content="<?php echo $ogImage?>" />