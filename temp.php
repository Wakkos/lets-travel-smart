<!doctype html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">

<title>Cheap flights</title>

<!-- http://bit.ly/18VB51x -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- CSS Para todos los navegadores -->
<link rel="stylesheet" href="css/style.css">

<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="css/lt-ie9.css">
<![endif]-->


<!-- Iconos -->



<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Windows 8 / RT http://bit.ly/HHkt7m -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">
<link href="https://fonts.googleapis.com/css?family=Didact+Gothic|PT+Serif:400,700" rel="stylesheet">
</head>
<body>
<?php
     $email = $_GET['email'];
     $servername = "localhost";
     $username = "wakkos";
     $password = "123456";
     $db = "fs";

     $conn = new mysqli($servername, $username, $password, $db);

     if ($conn->connect_errno) {
       printf("Falló la conexión: %s\n", $mysqli->connect_error);
       exit();
     }

     if (isset($_GET["email"]))
      {

        $user = $_GET["email"];
        echo $user;
        echo " is your email";
        // $sql = "INSERT INTO users (email) VALUES ('$user')";

        // if ($conn->query($sql) === TRUE) {
        //     echo "New record created successfully";
        // } else {
        //     echo "Error: " . $sql . "<br>" . $conn->error;
        // }
      }
      else
      {
        $user = null;
        echo "no email supplied";
      }
     ?>
    <section class="block block--small block--white">
        <h1 class="block__title">We need some quick input from you</h1>
        <div class="block__body">
            <form>
                <label for="Password">Your email</label>
                <input type="email" class="margin-bottom" value="<?php echo $user; ?>">
                <label for="Password">Create a Password</label>
                <input type="password" class="margin-bottom">
                <label for="service">What city or airport are you departing?</label>
                <small>We will look departures from that airport!</small>
                <div class="formgroup margin-bottom--mini">
                    <input type="text" size="50" id="js-userlocations" name="service" />
                    <span role="button" value="Add" id="js-addLocation" class="btn">Add</span>
                </div>
                <div id="js-suggestions" class="autocomplete"></div>
                <div class="taggroup" id="js-locations"></div>
                <div class="alert--block">
                    <div class="alert--block__icon">
                        <svg style="width:24px;height:24px" viewBox="0 0 24 24" class="iconsvg">
                            <path d="M13,14H11V10H13M13,18H11V16H13M1,21H23L12,2L1,21Z" />
                        </svg>
                    </div>
                    <p>You can only add one departure airport. You can many airports as you want than one airport with our Premium subscription.</p>
                    <div class="align-center">
                        <a href="#" class="btn--cta btn--small">Learn more</a>
                    </div>
                </div>
            </form>
        </div>
    </section>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">

//Autocomplete function
$(document).ready(function() {
    //Al escribr dentro del input con id="service"
    $('#js-userlocations').on('input',function(){
        //Obtenemos el value del input
        var service = $(this).val();
        var dataString = 'service='+service;

        //Le pasamos el valor del input al ajax
        $.ajax({
            type: "POST",
            url: "autocomplete.php",
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#js-suggestions').fadeIn(1000).html(data);
                $('#test').html(data);
                //Al hacer click en algua de las sugerencias
                $('#js-suggestions').on('click', '.suggest-element', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#js-userlocations').val(id);
                    //Hacemos desaparecer el resto de sugerencias
                    $('#js-suggestions').fadeOut(1000);
                });
            }
        });
    });
});

//Add location function

window.onload = function() {
    document.getElementById("js-addLocation").onclick = function addLocation() {
        var locationValue = document.getElementById('js-userlocations').value;

        var tag = document.createElement("span");


        tag.setAttribute("class", "badge");
        document.getElementById("js-locations").appendChild(tag);
        tag.innerHTML = locationValue;

        // Create hidden input
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "name_you_want");
        input.setAttribute("value", locationValue);
        document.getElementById("js-locations").appendChild(input);
    }
}
</script>
</body>
</html>