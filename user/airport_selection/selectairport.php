<?php
include_once "../../helper/session.php";
include_once '../../phpscript/conn.php';
include_once '../header.php';
?>



<?php



if (isset($_POST['iata'])) {
    $email = $_SESSION['email'];
    $user_id = $_SESSION['id'];
    $airportid = $_POST['iata'];
    $airporttomailchimp = false;
    $loginResult = "";

    if (isset($_POST['iata']) && is_array($_POST['iata'])) {
        foreach ($_POST['iata'] as $value) {
            $sql = "INSERT INTO userairports (airport_id, user_id)
                SELECT a.airport_id, u.user_id
                FROM users AS u
                CROSS JOIN airports AS a
                WHERE (u.user_email = '$email')
                AND (a.airport_id = '$value')
                AND not exists
					(SELECT * FROM userairports where (airport_id = '$value') and (user_id = '$user_id'))";
            if ($conn->query($sql) === true) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        $sql = "SELECT
            a.airport_iata, a.airport_id, u.user_id, u.airport_id
            FROM
                airports a
            INNER JOIN
                userairports u
            ON
                a.airport_id=u.airport_id
            WHERE
            u.user_id='$user_id'";
        $results = mysqli_query($conn, $sql);
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {

                while ($row = mysqli_fetch_array($result)) {
                    $airporttomailchimp .= $row['airport_iata'] . ",";
                }
            } else {
                $loginResult .= '<div class="autocomplete__item alert--warning"> No records matching your query were found.</div>';
            }
        } else {
            $loginResult .= "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }

        $apiKey = "7fb3ff4e766ffdfe332dbb81967330df-us2";
        $listID = "684c61e187";
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey, strpos($apiKey, '-') + 1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        // member information

        $json = json_encode([
            'merge_fields' => [
                'AIRPORTS' => $airporttomailchimp,
            ],
        ]);
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo "This: " . $airporttomailchimp . "-";
        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = '<p style="color: #34A853">You have successfully updated.</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = 'Some problem occurred, please try again.';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">' . $msg . '</p>';
        }

        header('location: /user/dashboard.php');
    } else {
        echo '<div class="alert--warning grid--item-12">You have to select at least one departure airport.</div>';
    }
}
?>

<?php
include_once "../../shared/topbar.php";
?>

<div class="breadcrumb">
    <a href="selectcountry.php" class="breadcrumb__item">Select Country</a>
     <span class="breadcrumb__item">Select Airport</span>
</div>

<div class="stickyfooter">
<h1 class="page-header">
Choose your airports.
</h1>
    <section class="max-width block margin-bottom padding" id="js-hasAlert">
        <h2 class="block__title">We will send you offers from the airports you choose.</h2>
        <form action=""  method="post">
            <?php
$user_id = $_SESSION['id'];
$airportIdInUser[] = array();
if (isset($_GET['country'])) {

    $country = $_GET['country'];
    $data = mysqli_query($conn, "SELECT country_hasZone, country_id from countries
                    WHERE country_code = '$country'");

    $row = mysqli_fetch_assoc($data);

    $countryData = $row['country_id'];
    $countryHasZone = $row['country_hasZone'];
    if ($countryHasZone) {
        ?>
                        <div class="grid bg-color-grey margin-bottom">
                        <?php
$zones = "select * from zones where country_id = '$countryData'";
        if ($result = mysqli_query($conn, $zones)) {
            while ($row = mysqli_fetch_array($result)) {
                echo "<a class=\"zonemap\" href=\"selectbyzone.php?country=" .$country."&&zone=" . $row['zone_id'] . "\">";
                echo "<img src=\"/resources/img/maps/map_" . $row['zone_id'] . ".svg\">";
                echo "<p>" . $row['zone_name'] . "</p>";
                echo "</a>";
            }
        }
        ?>
                        </div>
                        <?php
} else {
        echo "<div class=\"grid justify-spacebetween margin-bottom\">";
        $userAirports = " SELECT airport_id from userairports where user_id = '$user_id'";
        if ($result = mysqli_query($conn, $userAirports)) {
            while ($row = mysqli_fetch_array($result)) {
                $airportIdInUser[] = $row['airport_id'];
            }
        }
        $sql = "SELECT airport_id,airport_name, airport_iata, city_name FROM airports a
                                left join cities c on c.city_id = a.city_id
                                where c.country_id = '$countryData'";
        if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    if (in_array($row['airport_id'], $airportIdInUser)) {
                        $isChecked = "checked";
                    } else {
                        $isChecked = null;
                    }
                    echo "<div class='grid--item-6 checkbox margin-bottom'><input type='checkbox'
                                     " . $isChecked . " class='js-checkbox' name='iata[]'
                                     value='" . $row['airport_id'] . "'
                                     id='" . $row['airport_iata'] . "'><label for='" . $row['airport_iata'] . "'
                                     class='checkbox__label'><b>" . $row['airport_iata'] . " - " . $row['airport_name'] . "</b> - <span class='font-small'>" . $row['city_name'] . "</span></label>
                                     </div>";
                }

                // Free result set
                mysqli_free_result($result);
            } else {
                echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        echo "</div>";
    }

} else {
    echo "<div class=\"alert--warning\">You didn't select any airport.</div>";
}
?>
            <div class="align-right">
                <input type="submit" class="btn" value="add airports" id="js-addAirports">
            </div>
        </form>

    </section>

</div>



<?php
include_once "../../shared/footer.php";
?>


<script type="text/javascript">

    function checkbox(){

        if (document.querySelector('.js-checkbox:checked')) {
            console.log('foo')
        } else {
                var tag = document.createElement("div");
                tag.setAttribute("class", "airportBadge");
                tag.setAttribute("id", "js-airportBadge");
                tag.setAttribute("onclick", "DeleteBadge(this);");
                document.getElementById("js-hasAlert").appendChild(tag);
                tag.innerHTML = '<span class="alert--error">you have to select at least one airport.</span>';
            return false;
        }
    }

</script>
</body>
</html>